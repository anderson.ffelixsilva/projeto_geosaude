webpackJsonp([16],{

/***/ 123:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AboutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutPage');
    };
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\about\about.html"*/'<!--\n  Generated template for the SobrePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Sobre</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\about\about.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 124:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AchievementsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_chart_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AchievementsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AchievementsPage = /** @class */ (function () {
    function AchievementsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AchievementsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ConquistasPage');
        this.barChart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.barCanvas.nativeElement, {
            type: 'bar',
            data: {
                labels: ["Ibura", "Derby", "Pina", "Várzea", "Barro", "Torre"],
                datasets: [{
                        label: 'Nº de visitas',
                        data: [12, 19, 3, 5, 2, 3],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                },
                legend: {
                    onClick: ''
                }
            }
        });
        this.doughnutChart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.doughnutCanvas.nativeElement, {
            type: 'doughnut',
            data: {
                labels: ["Completo", "Incompleto"],
                datasets: [{
                        label: 'Percentual',
                        data: [63, 37],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)'
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ]
                    }]
            },
            options: {
                legend: {
                    onClick: ''
                }
            }
        });
        this.lineChart = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.lineCanvas.nativeElement, {
            type: 'line',
            data: {
                labels: ["Ciclo 1", "Ciclo 2", "Ciclo 3", "Ciclo 4", "Ciclo 5", "Ciclo 6", "Ciclo 7"],
                datasets: [
                    {
                        label: "Nº de ocorrências",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [65, 59, 80, 81, 56, 55, 40],
                        spanGaps: false,
                    }
                ]
            },
            options: {
                legend: {
                    onClick: ''
                }
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])('barCanvas'),
        __metadata("design:type", Object)
    ], AchievementsPage.prototype, "barCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], AchievementsPage.prototype, "doughnutCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])('lineCanvas'),
        __metadata("design:type", Object)
    ], AchievementsPage.prototype, "lineCanvas", void 0);
    AchievementsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-achievements',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\achievements\achievements.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n\n    <!-- Botão para o side menu -->\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n\n    <ion-title>Conquistas</ion-title>\n    \n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-card>\n    <ion-card-header>\n      Visitas\n    </ion-card-header>\n    <ion-card-content>\n      <canvas #barCanvas></canvas>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-header>\n      Ciclo(%)\n    </ion-card-header>\n    <ion-card-content>\n      <canvas #doughnutCanvas></canvas>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-header>\n      Ocorrências\n    </ion-card-header>\n    <ion-card-content>\n      <canvas #lineCanvas></canvas>\n    </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\achievements\achievements.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], AchievementsPage);
    return AchievementsPage;
}());

//# sourceMappingURL=achievements.js.map

/***/ }),

/***/ 125:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AedesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tratamento_psa_tratamento_psa__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__classes_VisitInformationPsa__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AedesPage = /** @class */ (function () {
    // ----------- Métodos -------------------
    function AedesPage(navCtrl, navParams, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.breendingGroundAedes = new __WEBPACK_IMPORTED_MODULE_3__classes_VisitInformationPsa__["a" /* BreendingGroundsAedes */]();
        this.visitInformation = this.navParams.get('visitInformation');
        console.log(this.visitInformation);
    }
    // Método executado no carregamento desta tela (AedesPage).
    AedesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AedesPage');
        // Obtendo o tipo do formulário que está sendo preenchido.
        this.formType = this.navParams.get("formType");
    };
    // Método para chamar a próxima tela.
    AedesPage.prototype.GotoNextPage = function () {
        console.log(this.visitInformation);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__tratamento_psa_tratamento_psa__["a" /* TratamentoPsaPage */], { foco: "Aedes", visitInformation: this.visitInformation, breendingGroundAedes: this.breendingGroundAedes });
    };
    // Método para volta a tela anterior.
    AedesPage.prototype.GoBacktoPageImovel = function () {
        this.navCtrl.pop();
    };
    // Finaliza o preenchimento devido o formulário ser LIRAa.
    AedesPage.prototype.Finish = function () {
        // Atribui o objeto com os dados de entrada ao objeto do VisitInformationLIRAa.
        this.visitInformation.setVtAedes(this.breendingGroundAedes);
        console.log(this.visitInformation);
        // Retorna a página StartForms.
        //this.navCtrl.setRoot(StartFormsPage, { pageFlag: "false", formName: this.formType, visitInformation: this.visitInformation, fieldAedesDone: true });
        //this.navCtrl.popToRoot();
        this.events.publish('fieldAedesDone', this.visitInformation);
        this.navCtrl.pop();
    };
    AedesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-aedes',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\aedes\aedes.html"*/'<!--\n  HTML DA TELA DE AEDES\n-->\n\n<ion-header>\n\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-label>\n\n    <h1 align="center">AEDES</h1>\n    <b></b>\n    <h2 align="center">Tipos de Criadouros</h2>\n\n  </ion-label>\n\n\n  <!-- Lista com as opções de Criadouros -->\n\n  <ion-list>\n\n\n    <ion-item>\n      <ion-label>A1 : </ion-label>\n      <ion-input type="number" [(ngModel)]="breendingGroundAedes.A1"></ion-input>\n      <button clear item-end ion-button tooltip="A1 - Caixa d\'água ligada à rede" positionV="bottom" arrow>\n        <ion-icon name="help-circle"></ion-icon>\n      </button>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>A2 : </ion-label>\n      <ion-input type="number" style="max-width: 90%" [(ngModel)]="breendingGroundAedes.A2"></ion-input>\n      <button clear item-end ion-button tooltip="A2 - Depósitos ao nível do solo, consumo doméstico (barril, tina, tonel, tambor, \n                                                depósito de barro, tanque, poço, cisterna e cacimba)" positionV="bottom"\n        arrow>\n        <ion-icon name="help-circle"></ion-icon>\n      </button>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>B  : </ion-label>\n      <ion-input type="number" style="max-width: 90%" [(ngModel)]="breendingGroundAedes.B"></ion-input>\n      <button clear item-end ion-button tooltip="B - Vasos/frascos com água, pratos, pingadeiras, recipientes de gelo, bebedouros em geral,\n                                                 pequenas fontes ornamentais, material depositado de construção, objetos religiosos/rituais"\n        positionV="bottom" arrow>\n        <ion-icon name="help-circle"></ion-icon>\n      </button>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>C  : </ion-label>\n      <ion-input type="number" style="max-width: 90%" [(ngModel)]="breendingGroundAedes.C"></ion-input>\n      <button clear item-end ion-button tooltip="C - Tanques em obras, borracharias e hortais, calhas, lajes e toldos em desníveis, ralos sanitários em desuso,\n                                                 obras arquitetônicas, piscinas não tratadas, fontes ornamentais, floreiras/vasos em cemitérios,\n                                                 cacos de vidro em muros, caixas de inspeção e passagem" positionV="bottom"\n        arrow>\n        <ion-icon name="help-circle"></ion-icon>\n      </button>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>D1 : </ion-label>\n      <ion-input type="number" style="max-width: 90%" [(ngModel)]="breendingGroundAedes.D1"></ion-input>\n      <button clear item-end ion-button tooltip="D1 - Pneus e outros materiais rodantes (manchões/câmaras)" positionV="bottom"\n        arrow>\n        <ion-icon name="help-circle"></ion-icon>\n      </button>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>D2 : </ion-label>\n      <ion-input type="number" style="max-width: 90%" [(ngModel)]="breendingGroundAedes.D2"></ion-input>\n      <button clear item-end ion-button tooltip="D2 - Lixo (recipientes plásticos, garrafas, latas), sucatas em pátios de ferro velho e recicladores, entulhos"\n        positionV="bottom" arrow>\n        <ion-icon name="help-circle"></ion-icon>\n      </button>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>E  : </ion-label>\n      <ion-input type="number" style="max-width: 90%" [(ngModel)]="breendingGroundAedes.E"></ion-input>\n      <button clear item-end ion-button tooltip="E - Axilas de folhas (bromélias, etc.), buracos em árvores e em rochas, cascas de animais (cascos e carapaças)"\n        positionV="bottom" arrow>\n        <ion-icon name="help-circle"></ion-icon>\n      </button>\n    </ion-item>\n\n\n  </ion-list>\n\n\n  <!-- Botões de Navegação -->\n\n  <div text-center>\n\n    <button ion-button icon-left (click)="GoBacktoPageImovel()" *ngIf="formType==\'PSA\'">\n      <ion-icon name="md-arrow-back"></ion-icon>\n      Voltar\n    </button>\n\n    <button ion-button icon-center (click)="GotoNextPage()" *ngIf="formType==\'PSA\'">\n      Próximo &nbsp;\n      <!-- &nbsp é para dar espaço -->\n      <ion-icon name="md-arrow-forward"></ion-icon>\n    </button>\n\n    <!--Finalizando -->\n    <button ion-button icon-right (click)="Finish()" *ngIf="formType==\'LIRAa\'">\n      Finalizar\n      <ion-icon name="exit"></ion-icon>\n    </button>\n\n  </div>\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\aedes\aedes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]])
    ], AedesPage);
    return AedesPage;
}());

//# sourceMappingURL=aedes.js.map

/***/ }),

/***/ 126:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StartVisitPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__select_form_select_form__ = __webpack_require__(127);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the StartVisitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var StartVisitPage = /** @class */ (function () {
    function StartVisitPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.items = [
            'Rua Marquês de Baipendi',
            'Rua Cirilino Afonso de Melo',
            'Rua Mário Sete',
            'Rua São Caetano',
            'Av. Agamenon Magalhães',
            'Rua Guaianazes',
            'Rua Esberard',
            'Rua Pereira Passos',
            'Rua Nova',
            'Rua Projetada',
            'Rua Ledinha'
        ];
        this.items2 = {
            sanitaryDistrict: "IV",
            strata: [
                "Ilha do Retiro e Madalena",
                "Cordeiro 1 e Zumbi",
                "Cordeiro 2",
                "Torre",
                "Prado",
                "Torrôes",
                "Engenho do Meio",
                "Iputinga 1",
                "Várzea 1 (CDU) / Várzea (Brasilit) / CDU",
                "Várzea 3 (Baixo) / Várzea 4 (Barreiras)",
                "Várzea 5 (UR-7)",
                "Caxangá",
                "Iputinga 2"
            ]
        };
    }
    StartVisitPage.prototype.ionViewDidLoad = function () {
    };
    StartVisitPage.prototype.itemSelected = function (item) {
        console.log("Selected Item", item);
    };
    StartVisitPage.prototype.GotoSelectForm = function (item) {
        console.log("Nome da rua escolhida : " + item);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__select_form_select_form__["a" /* SelectFormPage */], { streetName: item });
    };
    StartVisitPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-start-visit',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\start-visit\start-visit.html"*/'<!--\n  Generated template for the StartVisitPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Inicio da Visita</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-item class="sanitaryDistrictStyle">\n    Distrito Sanitário {{ items2.sanitaryDistrict }}\n  </ion-item>\n\n  <ion-item class="strataStyle">\n    Estratos para Visitar\n  </ion-item>\n\n  <ion-list inset>\n    <button ion-item *ngFor="let item of items2.strata" (click)="GotoSelectForm(item)">\n      {{ item }}\n      <ion-icon name="pin" item-end></ion-icon>\n    </button>\n  </ion-list>\n  \n<!--\n  <ion-list>\n    <ion-item-sliding *ngFor="let item of items2.strata">\n      <ion-item>\n        <ion-card>\n          <button ion-item class="button-custom">{{ item }}\n              <ion-icon name="pin" item-end></ion-icon>\n          </button>      \n        </ion-card>\n      </ion-item>\n    </ion-item-sliding>\n  </ion-list>\n\n  -->\n</ion-content>\n'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\start-visit\start-visit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], StartVisitPage);
    return StartVisitPage;
}());

//# sourceMappingURL=start-visit.js.map

/***/ }),

/***/ 127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectFormPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__start_forms_start_forms__ = __webpack_require__(128);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SelectFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SelectFormPage = /** @class */ (function () {
    //------------- Métodos ---------------------------
    function SelectFormPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SelectFormPage.prototype.ionViewDidLoad = function () {
        // Recebendo o parâmetro passado - nome da rua.
        this.streetName = this.navParams.get("streetName");
    };
    // Abrindo a página de StartForms e passando o nome do formulário escolhido (Liraa ou PSA).
    SelectFormPage.prototype.GotoStartFormsPage = function (formType) {
        // Fazendo a concatenção do nome da rua com o endereço do imóvel, para ser armazenado 
        this.address = this.streetName + ', ' + this.address;
        console.log("Endereço concatenado: " + this.address);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__start_forms_start_forms__["a" /* StartFormsPage */], { formName: formType, address: this.address });
    };
    SelectFormPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-select-form',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\select-form\select-form.html"*/'<!--\n  Generated template for the SelectFormPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>Seleção do Formulário</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-label class="address_name">\n    Endereço\n  </ion-label>\n  <ion-card class="card_settings">\n    <ion-input type="text" value="Nº 2059 Apartamento 101" [(ngModel)]="address"></ion-input>\n  </ion-card>\n\n  <!-- Formulários -->\n  <br>\n  <br>\n  <ion-label class="forms_names">\n    Formulários Disponíveis\n  </ion-label>\n\n  <div class="buttons">\n    <button ion-button (click)="GotoStartFormsPage(\'PSA\')" color="primary" block class="button_psa">\n      Formulário 1 - Índice PSA\n    </button>\n\n    <button ion-button (click)="GotoStartFormsPage(\'LIRAa\')" color="primary" block class="button_liraa">\n      Formulário 2 - LIRAa\n    </button>\n  </div>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\select-form\select-form.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], SelectFormPage);
    return SelectFormPage;
}());

//# sourceMappingURL=select-form.js.map

/***/ }),

/***/ 128:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StartFormsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__activities_activities__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__immobile_immobile__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__aedes_aedes__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__culex_culex__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__trash_trash__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_psa_form_psa_form__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__larvicides_larvicides__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_liraa_form_liraa_form__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__classes_VisitInformationLiraa__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__classes_VisitInformationPsa__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_api_api__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_api_send_forms_api_send_forms__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_network_network__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var StartFormsPage = /** @class */ (function () {
    // -------------- Métodos -----------------------------
    function StartFormsPage(events, navCtrl, navParams, alertCtrl, psaFormProvider, liraaFormProvider, apiProvider, apiSendFormsProvider, networkProvider, toastCtrl) {
        // Os eventos abaixo, são responsáveis por realizar a troca do ícone que fica ao lado de cada campo, de forma a confirmar o preenchimendo do campo.
        var _this = this;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.psaFormProvider = psaFormProvider;
        this.liraaFormProvider = liraaFormProvider;
        this.apiProvider = apiProvider;
        this.apiSendFormsProvider = apiSendFormsProvider;
        this.networkProvider = networkProvider;
        this.toastCtrl = toastCtrl;
        this.fieldImmobileDone = false;
        this.fieldTrashDone = false;
        this.fieldAedesDone = false;
        this.fieldCulexDone = false;
        this.fieldOvitrapsDone = false;
        this.fieldLarvicidesDone = false;
        this.fieldDepositsDone = false;
        // Aguarda o evento, fieldImmobileDone, ser publicado. Para alterar o valor do atributo fieldImmobileDone, alterando o ícone de seta para checkmark.
        events.subscribe('fieldImmobileDone', function (visitInformationImmobile) {
            _this.fieldImmobileDone = true;
            _this.visitInformation = visitInformationImmobile;
        });
        // Aguardo o evento, fieldTrashDone, ser publicado/realizado. Para alterar o valor do atributo fieldTrashDone, alterando o ícone de seta para checkmark.
        events.subscribe('fieldTrashDone', function (visitInformationTrash) {
            _this.fieldTrashDone = true;
            _this.visitInformation = visitInformationTrash;
        });
        // Aguarda o evento, fieldAedesDone, ser publicado/realizado. Para alterar o valor do atributo fieldAedesDone, alterando o ícone de seta para checkmark.
        events.subscribe('fieldAedesDone', function (visitInformationAedes) {
            _this.fieldAedesDone = true;
            _this.visitInformation = visitInformationAedes;
        });
        // Aguarda o evento, fieldCulexDone, ser publicado/realizado. Para alterar o valor do atributo fieldCulexDone, alterando o ícone de seta para checkmark
        events.subscribe('fieldCulexDone', function (visitInformationCulex) {
            _this.fieldCulexDone = true;
            _this.visitInformation = visitInformationCulex;
        });
        // Aguarda o evento, fieldOvitrapsDone, ser publicado/realizado. Para alterar o valor do atributo fieldOvitraps, alterando o ícone de seta para checkmark.
        events.subscribe('fieldOvitrapsDone', function (visitInformationOvitraps) {
            _this.fieldOvitrapsDone = true;
            _this.visitInformation = visitInformationOvitraps;
        });
        // Aguarda o evento, fieldLarvicidesDone, ser publicado/realizado. Para alterar o valor do atributo fieldLarvicidesDone, alterando o ícone de seta para checkmark.
        events.subscribe('fieldLarvicidesDone', function (visitInformationLarvicides) {
            _this.fieldLarvicidesDone = true;
            _this.visitInformation = visitInformationLarvicides;
        });
        // Aguarda o evento, fieldDepositsDone, ser publicado/realizado. Para alterar o valor do atributo fieldDepositsDone, alterando o ícone de seta para checkmark.
        events.subscribe('fieldDepositsDone', function (visitInformationDeposits) {
            _this.fieldDepositsDone = true;
            _this.visitInformation = visitInformationDeposits;
        });
    }
    // ionViewDidLoad executa quando a página é carregada. Esse evento acontece apenas uma vez quando a página é criada.
    StartFormsPage.prototype.ionViewDidLoad = function () {
        //Recebe informação sobre o tipo de formulario selecionado: LIRAa ou PSA. E sobre o endereço.
        this.formType = this.navParams.get("formName");
        this.address = this.navParams.get("address");
        // Caso o preenchimento do formulário já tenha sido iniciado, oculta o botão de voltar para a página de SelectForm
        /*if (this.navParams.get("pageFlag")) {
          this.visitInformation = this.navParams.get("visitInformation");
          this.existsPage = this.navParams.get("pageFlag");
        } else {
          this.existsPage = "true";
        }*/
        // Verificando o tipo do formulário para criar a instância de acordo com o formulário escolhido.
        if (this.formType == 'PSA') {
            this.visitInformation = new __WEBPACK_IMPORTED_MODULE_11__classes_VisitInformationPsa__["g" /* VisitInformationPSA */](null, null, null, null, null, null, null); // Criando a instância para o formulário PSA.
            this.visitInformation.setPlace(this.address); // Adicionando o endereço ao campo places.
        }
        else if (this.formType == 'LIRAa') {
            console.log("Criando formulário liraa");
            "";
            this.visitInformation = new __WEBPACK_IMPORTED_MODULE_10__classes_VisitInformationLiraa__["c" /* VisitInformationLIRAa */](null, null, null, null, null); // Criando a instância para o formulário LIRAa.
            //this.visitInformation.places = this.address;            // Adicionando o endereço ao campo places.
            this.visitInformation.setPlace(this.address);
        }
    };
    ;
    // Método que chama a tela do Imóvel (ImmobilePage).
    StartFormsPage.prototype.GotoImovelPage = function () {
        console.log(this.visitInformation);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__immobile_immobile__["a" /* ImmobilePage */], { formType: this.formType, visitInformation: this.visitInformation, context: this });
    };
    // Método que chama a tela referentes aos lixos encontrados (LixoPage).
    StartFormsPage.prototype.GotoLixoPage = function () {
        console.log("passagem imovel: " + this.formType);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__trash_trash__["a" /* TrashPage */], { formType: this.formType, visitInformation: this.visitInformation });
    };
    // Método que chama a tela do Aedes (AedesPage).
    StartFormsPage.prototype.GotoAedesPage = function () {
        console.log("passagem imovel: " + this.formType);
        console.log(this.visitInformation);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__aedes_aedes__["a" /* AedesPage */], { formType: this.formType, visitInformation: this.visitInformation });
    };
    // Método que chama a tela do Cúlex (CulexPage).
    StartFormsPage.prototype.GotoCulexPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__culex_culex__["a" /* CulexPage */], { visitInformation: this.visitInformation });
    };
    // Método que abre a tela referente aos larvicidas.
    StartFormsPage.prototype.GotoLarvicidasPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__larvicides_larvicides__["a" /* LarvicidesPage */], { formType: this.formType, visitInformation: this.visitInformation });
    };
    // Número de ovitrampas - Para o formulário PSA.
    StartFormsPage.prototype.GotoOvitrampasEvent = function () {
        // Coletando o número de ovitrampas por meio de um Alert.
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Digite o Número de Ovitrampas: ',
            inputs: [
                {
                    name: 'ovitrampas',
                    placeholder: 'Ovitrampas',
                    type: 'number'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function () {
                        console.log("Clicou em cancelar");
                    }
                },
                {
                    text: 'Ok',
                    role: 'ok',
                    handler: function (data) {
                        _this.visitInformation.setOvitraps(data.ovitrampas);
                        _this.fieldOvitrapsDone = true;
                    }
                }
            ]
        });
        alert.present();
    };
    // Número de depósitos eliminados - Formulário LIRAa.
    StartFormsPage.prototype.GotoDepositosEvent = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Número de Depósitos Eliminados: ',
            inputs: [
                {
                    name: 'depositos',
                    placeholder: 'Depósitos Eliminados'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function () {
                        console.log("Clicou em cancelar");
                    }
                },
                {
                    text: 'Ok',
                    role: 'ok',
                    handler: function (data) {
                        _this.visitInformation.setDepositsRemoved(data.depositos); // Obtém o que foi digitado e armazena no objeto VisitInformationLIRAa.
                        _this.fieldDepositsDone = true;
                    }
                }
            ]
        });
        alert.present();
    };
    // Volta para a tela de seleção do formulário (SelectFormPage).
    StartFormsPage.prototype.GotoBackSelectFormPage = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Cancelar Preenchimento do Formulário',
            message: 'Você tem certeza que deseja cancelar o preenchimento do formulário?',
            buttons: [
                {
                    text: 'Não',
                    role: 'dontCancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        _this.navCtrl.pop();
                    }
                }
            ]
        });
        alert.present();
    };
    // Método que finalizar a visita
    StartFormsPage.prototype.GotoFinalizeVisit = function () {
        var _this = this;
        // Confirma se o usuário deseja encerrar o formulário.
        var alert = this.alertCtrl.create({
            title: 'Concluir o Preenchimento do Formulário',
            message: 'Deseja concluir o preenchimento do formulário?',
            buttons: [
                {
                    text: 'Não',
                    role: 'dontCancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        // Salvando as informações no armazenamento (storage).
                        if (_this.formType == 'PSA') {
                            // Se tiver conexão com a internet, envia dados ao servidor.
                            if (_this.networkProvider.getNetworkStatus() == "online") {
                                _this.apiSendFormsProvider.sendFormPsa(_this.visitInformation) // Enviando ao Servidor.
                                    .then(function (res) {
                                    _this.presentAlert('Visita Finalizada com Sucesso', 'Dados enviados ao servidor!');
                                    console.log("PSA: enviou os dados ao server");
                                })
                                    .catch(function (err) {
                                    _this.psaFormProvider.insert(_this.visitInformation).then(function () {
                                        _this.events.publish('haveNewFormsToSend', true);
                                        console.log("PSA: não conseguiu enviar os dados ao server");
                                        _this.presentAlert('Visita Finalizada com Sucesso', '');
                                    }); // Inserindo no Storage.
                                });
                                // Caso não tenha conexão com a internet, armazena localmente.  
                            }
                            else {
                                _this.psaFormProvider.insert(_this.visitInformation).then(function () {
                                    _this.events.publish('haveNewFormsToSend', true);
                                    console.log("PSA: não conseguiu enviar os dados ao server, servidor off");
                                    _this.presentAlert('Visita Finalizada com Sucesso', '');
                                }); // Inserindo no Storage.
                            }
                            /*let autToken = this.apiProvider.getAuthenticationToken();
              
                            this.apiProvider.sendPSAFormToBackEnd(this.visitInformation, autToken.toString()).subscribe(
                              data => {
                                console.log(data);
              
                              }, error => {
                                console.log(error);
                              }
                               
                            );
                              */
                        }
                        else {
                            // Se tiver conexão com a internet, envia dados ao servidor.
                            if (_this.networkProvider.getNetworkStatus() == "online") {
                                _this.apiSendFormsProvider.sendFormLiraa(_this.visitInformation) // Enviando ao Servidor.
                                    .then(function (res) {
                                    _this.presentAlert('Visita Finalizada com Sucesso', 'Dados enviados ao servidor!');
                                    console.log("LIRAA: enviou os dados ao server");
                                })
                                    .catch(function (err) {
                                    _this.liraaFormProvider.insert(_this.visitInformation).then(function () {
                                        _this.events.publish('haveNewFormsToSend', true);
                                        console.log("LIRAA: não conseguiu enviar os dados ao server");
                                        _this.presentAlert('Visita Finalizada com Sucesso', '');
                                    }); // Inserindo no Storage.
                                });
                                // Caso não tenha conexão com a internet, armazena localmente.  
                            }
                            else {
                                // Inserindo no Storage.
                                _this.liraaFormProvider.insert(_this.visitInformation).then(function () {
                                    _this.events.publish('haveNewFormsToSend', true);
                                    console.log("LIRAA: não conseguiu enviar os dados ao server, servidor off");
                                    _this.presentAlert('Visita Finalizada com Sucesso', '');
                                });
                            }
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    StartFormsPage.prototype.presentAlert = function (titleMsg, subTitleMsg) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: titleMsg,
            subTitle: subTitleMsg,
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                        _this.events.publish('newForms:true');
                        console.log("encerrando a visita");
                        //Retira as páginas anteriores da pilha e volta para a TabsPage.
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__activities_activities__["a" /* ActivitiesPage */]);
                        _this.navCtrl.popToRoot();
                    }
                }
            ]
        });
        alert.present();
    };
    StartFormsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-start-forms',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\start-forms\start-forms.html"*/'\n<ion-header >\n    <ion-navbar color="primary"  hideBackButton >\n  \n      <ion-title *ngIf="formType==\'LIRAa\'">\n        Boletim do Índice LIRAa\n      </ion-title>\n  \n      <ion-title *ngIf="formType==\'PSA\'">\n        Boletim de Visita Diária PSA\n      </ion-title>\n  \n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content padding>\n  \n    <ion-card>\n  \n      <ion-list>\n  \n        <button ion-item (click)="GotoImovelPage()">\n          <ion-icon name="home" item-start></ion-icon>\n          <ion-icon name="arrow-forward" item-end *ngIf="fieldImmobileDone==false"></ion-icon>\n          <ion-icon name="md-checkmark-circle-outline" color="green" item-end *ngIf="fieldImmobileDone==true"></ion-icon>\n          Imóvel\n        </button>\n  \n        <button ion-item (click)="GotoLixoPage()" *ngIf="formType==\'PSA\'">\n          <ion-icon name="trash" item-start></ion-icon>\n          <ion-icon name="arrow-forward" *ngIf="fieldTrashDone==false" item-end></ion-icon>\n          <ion-icon name="md-checkmark-circle-outline" color="green" item-end *ngIf="fieldTrashDone==true"></ion-icon>\n          Lixo\n        </button>\n  \n        <button ion-item (click)="GotoAedesPage()">\n          <ion-icon name="bug" item-start></ion-icon>\n          <ion-icon name="arrow-forward" *ngIf="fieldAedesDone==false" item-end></ion-icon>\n          <ion-icon name="md-checkmark-circle-outline" color="green" item-end *ngIf="fieldAedesDone==true"></ion-icon>\n          Vetor-Aedes\n        </button>\n  \n        <button ion-item (click)="GotoCulexPage()" *ngIf="formType==\'PSA\'">\n          <ion-icon name="bug" item-start></ion-icon>\n          <ion-icon name="arrow-forward" *ngIf="fieldCulexDone==false" item-end></ion-icon>\n          <ion-icon name="md-checkmark-circle-outline" color="green" item-end *ngIf="fieldCulexDone==true"></ion-icon>\n          Vetor-Cúlex\n        </button>\n  \n        <button ion-item (click)="GotoOvitrampasEvent()" *ngIf="formType==\'PSA\'">\n          <ion-icon name="cube" item-start></ion-icon>\n          <ion-icon name="arrow-forward" *ngIf="fieldOvitrapsDone==false" item-end></ion-icon>\n          <ion-icon name="md-checkmark-circle-outline" color="green" item-end *ngIf="fieldOvitrapsDone==true"></ion-icon>\n          Ovitrampas\n        </button>\n\n        <button ion-item (click)="GotoDepositosEvent()" *ngIf="formType==\'LIRAa\'">\n            <ion-icon name="cube" item-start></ion-icon>\n            <ion-icon name="arrow-forward" *ngIf="fieldDepositsDone==false" item-end></ion-icon>\n          <ion-icon name="md-checkmark-circle-outline" color="green" item-end *ngIf="fieldDepositsDone==true"></ion-icon>\n            Depósitos Eliminados\n          </button>\n  \n        <button ion-item (click)="GotoLarvicidasPage()">\n          <ion-icon name="md-color-filter" item-start></ion-icon>\n          <ion-icon name="arrow-forward" *ngIf="fieldLarvicidesDone==false" item-end></ion-icon>\n          <ion-icon name="md-checkmark-circle-outline" color="green" item-end *ngIf="fieldLarvicidesDone==true"></ion-icon>\n          Larvicidas\n        </button>\n      </ion-list>\n  \n    </ion-card>\n  \n    <br>\n    <br>\n  \n    <!-- Botões de voltar e finalizar a visita -->\n  \n    <div text-center>\n  \n      <button ion-button icon-left (click)="GotoBackSelectFormPage()" >\n        <ion-icon name="md-arrow-back"></ion-icon>\n        Voltar\n      </button>\n  \n      <button ion-button icon-center (click)="GotoFinalizeVisit()">\n        Finalizar Visita &nbsp;\n        <!-- &nbsp é para dar espaço -->\n        <ion-icon name="exit" item-end></ion-icon>\n      </button>\n  \n    </div>\n  \n  </ion-content>'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\start-forms\start-forms.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_psa_form_psa_form__["a" /* PsaFormProvider */],
            __WEBPACK_IMPORTED_MODULE_9__providers_liraa_form_liraa_form__["a" /* LiraaFormProvider */],
            __WEBPACK_IMPORTED_MODULE_12__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_13__providers_api_send_forms_api_send_forms__["a" /* ApiSendFormsProvider */],
            __WEBPACK_IMPORTED_MODULE_14__providers_network_network__["a" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */]])
    ], StartFormsPage);
    return StartFormsPage;
}());

//# sourceMappingURL=start-forms.js.map

/***/ }),

/***/ 129:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImmobilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__classes_VisitInformationPsa__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__classes_VisitInformationLiraa__ = __webpack_require__(96);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ImmobilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ImmobilePage = /** @class */ (function () {
    // ---------------- Métodos ---------------
    function ImmobilePage(navCtrl, navParams, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.visitInformation = this.navParams.get("visitInformation");
        console.log(JSON.stringify(this.visitInformation));
    }
    ImmobilePage.prototype.ionViewDidLoad = function () {
        //Recebe informação sobre o tipo de formulario selecionado: LIRAa ou PSA.
        this.formType = this.navParams.get("formType");
        // Instância objeto de acordo com o tipo do formulário.
        if (this.formType == 'PSA') {
            this.immobileInformation = new __WEBPACK_IMPORTED_MODULE_2__classes_VisitInformationPsa__["c" /* Immobile */]();
        }
        else {
            this.immobileInformation = new __WEBPACK_IMPORTED_MODULE_3__classes_VisitInformationLiraa__["a" /* ImmobileLIRAa */]();
        }
    };
    // Volta para a tela StartForms.
    ImmobilePage.prototype.GotoBackSelectFormPage = function () {
        this.navCtrl.pop();
    };
    // Método que finalizar a visita
    ImmobilePage.prototype.GotoFinalizeVisit = function () {
        this.immobileInformation.dateVisit = this.date;
        //this.visitInformation.immobile =  this.immobileInformation;
        this.visitInformation.setImmobile(this.immobileInformation);
        console.log("---- ImmobilePage:    ");
        console.log(JSON.stringify(this.visitInformation));
        //Retira as páginas anteriores da pilha e volta para a StartForms.
        //this.navCtrl.setRoot(StartFormsPage, { formName: this.formType, pageFlag: "false", visitInformation: this.visitInformation, fieldImmobileDone: true });
        //this.navCtrl.popToRoot();
        this.events.publish('fieldImmobileDone', this.visitInformation); // Publicando um evento para informar que campo imóvel foi concluído.
        this.navCtrl.pop();
    };
    ImmobilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-immobile',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\immobile\immobile.html"*/'<!--\n  Generated template for the ImovelPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  \n</ion-header>\n\n<ion-content padding>\n\n  <ion-label>\n    <h1 align="center">IMÓVEL</h1>\n    <b></b>\n    <h5 align="center">Informações Sobre o Imóvel</h5>\n  </ion-label>\n\n  <ion-card>\n\n\n    <!-- Endereço do imóvel -->\n\n    <ion-card-header>\n      <ion-input type="text" value="Nº 2059 Apartamento 101"></ion-input>\n    </ion-card-header>\n\n\n    <!-- Select com o tipo do imovel-->\n\n    <ion-list>\n      <ion-item *ngIf="formType==\'PSA\'">\n        <ion-label>Tipo de Imóvel</ion-label>\n        <ion-select [(ngModel)]="immobileInformation.tpImmobile">\n          <ion-option value="Residence">Residência</ion-option>\n          <ion-option value="commerce">Comércio</ion-option>\n          <ion-option value="industry">Indústria</ion-option>\n          <ion-option value="private service">Serviço Privado</ion-option>\n          <ion-option value="residence with commerce">Residência com Comércio</ion-option>\n          <ion-option value="immobile under construction">Imóvel em Construção</ion-option>\n          <ion-option value="wasteland">Terreno Baldio</ion-option>\n          <ion-option value="strategic point">Ponto Estratégico</ion-option>\n          <ion-option value="others">Outros</ion-option>\n        </ion-select>\n      </ion-item>\n    </ion-list>\n\n\n    <!--Select com o tipo da situação-->\n\n    <ion-list>\n      <ion-item *ngIf="formType==\'PSA\'">\n        <ion-label>Situação</ion-label>\n        <ion-select [(ngModel)]="immobileInformation.situationImmobile">\n          <ion-option value="inspected">Inspecionado</ion-option>\n          <ion-option value="not inspected">Não Inspecionado</ion-option>\n          <ion-option value="recovered">Recuperado</ion-option>\n        </ion-select>\n      </ion-item>\n    </ion-list>\n\n\n    \n    <!-- Select com o tipo do imovel-->\n\n    <ion-list>\n        <ion-item *ngIf="formType==\'LIRAa\'">\n          <ion-label>Tipo de Imóvel</ion-label>\n          <ion-select [(ngModel)]="immobileInformation.tpImmobile">\n            <ion-option value="residence">Residência</ion-option>\n            <ion-option value="commerce">Comércio</ion-option>\n            <ion-option value="others">Outros</ion-option>\n            <ion-option value="wasteland">Terreno baldio</ion-option>\n          </ion-select>\n        </ion-item>\n      </ion-list>\n\n\n    <!--Data da visita -->\n\n    <ion-item>\n      <ion-label>Data</ion-label>\n      <ion-datetime displayFormat="MMM DD YYYY" [(ngModel)]="date"></ion-datetime>\n    </ion-item>\n\n  </ion-card>\n\n\n  <!-- Botões de voltar e finalizar a visita -->\n\n  <div text-center>\n\n    <button ion-button icon-center (click)="GotoFinalizeVisit()">\n      Concluir &nbsp;\n      <!-- &nbsp é para dar espaço -->\n      <ion-icon name="exit"></ion-icon>\n    </button>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\immobile\immobile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]])
    ], ImmobilePage);
    return ImmobilePage;
}());

//# sourceMappingURL=immobile.js.map

/***/ }),

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CulexPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tratamento_psa_tratamento_psa__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__classes_VisitInformationPsa__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CulexPage = /** @class */ (function () {
    // ------------ Métodos ----------------
    function CulexPage(navCtrl, navParams, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.breendingGroundCulex = new __WEBPACK_IMPORTED_MODULE_3__classes_VisitInformationPsa__["b" /* BreendingGroundsCulex */]();
        this.visitInformation = this.navParams.get('visitInformation');
        console.log(this.visitInformation);
    }
    CulexPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CulexPage');
    };
    // Método para voltar a tela anterior.
    CulexPage.prototype.GoBack = function () {
        this.navCtrl.pop();
    };
    // Método para ir a próxima tela.
    CulexPage.prototype.GotoNextPage = function () {
        console.log(this.visitInformation);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__tratamento_psa_tratamento_psa__["a" /* TratamentoPsaPage */], { foco: 'Culex', visitInformation: this.visitInformation, breendingGroundCulex: this.breendingGroundCulex });
    };
    CulexPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-culex',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\culex\culex.html"*/'<ion-header>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n  <ion-label>\n    <h1 align="center">CÚLEX</h1>\n    <h2 align="center">Tipos de Criadouros</h2>\n  </ion-label>\n  \n\n  <!-- Lista com os tipos de criadouros -->\n\n  <ion-list>\n  \n  \n    <ion-item>\n      <ion-label>Fossa</ion-label>\n      <ion-checkbox checked="false" [(ngModel)]="breendingGroundCulex.fosse"></ion-checkbox>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>Cisterna</ion-label>\n      <ion-checkbox checked="false" [(ngModel)]="breendingGroundCulex.cistern"></ion-checkbox>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>Canal/Canaletas/Valas</ion-label>\n      <ion-checkbox checked="false" [(ngModel)]="breendingGroundCulex.channelDichGutters"></ion-checkbox>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>Charco</ion-label>\n      <ion-checkbox checked="false" [(ngModel)]="breendingGroundCulex.puddle"></ion-checkbox>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>Caixa de Inspeção/Drenagem</ion-label>\n      <ion-checkbox checked="false" [(ngModel)]="breendingGroundCulex.inspectionBoxDrainage"></ion-checkbox>\n    </ion-item>\n\n\n  </ion-list>\n\n\n  <!-- Botões de voltar e avançar -->\n\n  <div text-center>\n\n    <button ion-button icon-left (click)="GoBack()">\n      <ion-icon name="md-arrow-back"></ion-icon>\n      Voltar\n    </button>\n\n    <button ion-button icon-right (click)="GotoNextPage()">\n      Próximo\n      <ion-icon name="md-arrow-forward"></ion-icon>\n    </button>\n\n  </div>\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\culex\culex.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]])
    ], CulexPage);
    return CulexPage;
}());

//# sourceMappingURL=culex.js.map

/***/ }),

/***/ 131:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrashPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__classes_VisitInformationPsa__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TrashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TrashPage = /** @class */ (function () {
    function TrashPage(navCtrl, navParams, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.trashInformation = new __WEBPACK_IMPORTED_MODULE_2__classes_VisitInformationPsa__["e" /* Trash */]();
        this.visitInformation = this.navParams.get('visitInformation');
        console.log(this.visitInformation);
    }
    TrashPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TrashPage');
        //Recebe informação sobre o tipo de formulario selecionado: LIRAa ou PSA.
        this.formType = this.navParams.get("formType");
    };
    // Método que finalizar o preenchimento das informações de lixo
    TrashPage.prototype.GotoFinalizeVisit = function () {
        this.visitInformation.setTrash(this.trashInformation);
        console.log("------ TrashPage:   ");
        console.log(this.visitInformation);
        //Retira as páginas anteriores da pilha e volta para a TabsPage.
        //this.navCtrl.setRoot(StartFormsPage, { formName: this.formType, visitInformation: this.visitInformation, pageFlag: "false", fieldTrashDone: true });
        //this.navCtrl.popToRoot();
        this.events.publish('fieldTrashDone', this.visitInformation);
        this.navCtrl.pop();
    };
    TrashPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-trash',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\trash\trash.html"*/'<!--\n  Tela referente as informações de lixo do boletim de visita diária PSA.\n  Criado: 20/03/2018\n  Aplicativo: Geo-Saúde\n-->\n\n\n<ion-header>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-label>\n    <h1 align="center">LIXO</h1>\n    <h5 align="center">Informações Sobre o Lixo Encontrado</h5>\n  </ion-label>\n\n  <ion-card>\n\n    <ion-item>\n      <ion-label>Tipo</ion-label>\n      <ion-select [(ngModel)]="trashInformation.tpTrash">\n        <ion-option value="domicile">Domiciar</ion-option>\n        <ion-option value="special">Especial</ion-option>\n      </ion-select>\n    </ion-item>\n\n    <ion-item>\n      <ion-label>Acondicionamento</ion-label>\n      <ion-select [(ngModel)]="trashInformation.packaging">\n        <ion-option value="appropriate">Adequado</ion-option>\n        <ion-option value="inappropriate">Inadequado</ion-option>\n        <ion-option value="not applicable">Não se aplica</ion-option>\n      </ion-select>\n    </ion-item>\n\n    <ion-item>\n      <ion-label>Se domiciliar, qual destino?</ion-label>\n      <ion-select [(ngModel)]="trashInformation.domicileTrashDestiny">\n        <ion-option value="directly collect">Coletado diretamente</ion-option>\n        <ion-option value="indirectly collect">Coletado indiretamente</ion-option>\n        <ion-option value="burned and buried">Queimado e enterrado</ion-option>\n        <ion-option value="throw on barrier">Jogado em barreira</ion-option>\n        <ion-option value="throw on wasteland">Jogado em terreno baldio</ion-option>\n        <ion-option value="throw on the river, lake, sea or mangrove">Jogado no rio, lago, mar ou mangue</ion-option>\n        <ion-option value="throw in the gallery, duct or channel">Jogado na galeria, canal ou canaleta</ion-option>\n        <ion-option value="other">Outro</ion-option>\n        <ion-option value="selective collect">Coleta seletiva</ion-option>\n        <ion-option value="note applicable">Não se aplica</ion-option>\n      </ion-select>\n    </ion-item>\n\n  </ion-card>\n\n\n  <!-- Botões de voltar e finalizar a visita -->\n\n  <div text-center>\n\n    <button ion-button icon-center (click)="GotoFinalizeVisit()">\n      Concluir &nbsp;\n      <!-- &nbsp é para dar espaço -->\n      <ion-icon name="exit"></ion-icon>\n    </button>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\trash\trash.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]])
    ], TrashPage);
    return TrashPage;
}());

//# sourceMappingURL=trash.js.map

/***/ }),

/***/ 132:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LarvicidesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__classes_VisitInformationPsa__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__classes_VisitInformationLiraa__ = __webpack_require__(96);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LarvicidesPage = /** @class */ (function () {
    // ------------ Métodos ---------------
    function LarvicidesPage(navCtrl, navParams, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.formType = this.navParams.get("formType");
        // Instânciando objeto de acordo com o tipo do formulário.
        if (this.formType == 'PSA') {
            this.larvicidesInformation = new __WEBPACK_IMPORTED_MODULE_2__classes_VisitInformationPsa__["d" /* TpLarvicides */]();
        }
        else {
            this.larvicidesInformation = new __WEBPACK_IMPORTED_MODULE_3__classes_VisitInformationLiraa__["b" /* TpLarvicidesLIRAa */]();
        }
        this.visitInformation = this.navParams.get('visitInformation');
        console.log(this.visitInformation);
    }
    LarvicidesPage.prototype.ionViewDidLoad = function () {
    };
    LarvicidesPage.prototype.ionViewWillEnter = function () {
        console.log("tratamento formtype: " + this.formType);
    };
    /*
    GoBack(){
      this.navCtrl.pop();
    }
    */
    LarvicidesPage.prototype.Finish = function () {
        this.visitInformation.setLarvicides(this.larvicidesInformation);
        console.log(this.visitInformation);
        //Retira as páginas anteriores da pilha e volta para a TabsPage.
        //this.navCtrl.setRoot(StartFormsPage, { pageFlag: "false", formName: this.formType, visitInformation: this.visitInformation, fieldLarvicidesDone: true });
        //this.navCtrl.popToRoot();
        this.events.publish('fieldLarvicidesDone', this.visitInformation);
        this.navCtrl.pop();
    };
    LarvicidesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-larvicides',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\larvicides\larvicides.html"*/'<ion-header>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-label text-center>\n    <h1>Larvicidas</h1>\n    <h5>Informações Sobre os Larvicidas</h5>\n  </ion-label>\n\n  \n  <ion-list>\n    <ion-label>\n      <h4>BTI G</h4>\n    </ion-label>\n\n    <ion-item>\n      <ion-label>Gramas</ion-label>\n      <ion-input type="number" [(ngModel)]="larvicidesInformation.BTiG_gramas"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>Depósito</ion-label>\n      <ion-input type="number" [(ngModel)]="larvicidesInformation.BTiG_depositos"></ion-input>\n    </ion-item>\n\n    <ion-label>\n      <h4>BTI WDg</h4>\n    </ion-label>\n\n    <ion-item>\n      <ion-label>Gramas</ion-label>\n      <ion-input type="number" [(ngModel)]="larvicidesInformation.BTiWDg_gramas"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>Depósito</ion-label>\n      <ion-input type="number" [(ngModel)]="larvicidesInformation.BTiWDg_depositos"></ion-input>\n    </ion-item>\n  </ion-list>\n\n  <ion-list *ngIf="formType==\'PSA\'">\n    <ion-label>\n      <h4>BsG</h4>\n    </ion-label>\n    <ion-item>\n      <ion-label>Gramas</ion-label>\n      <ion-input type="number" [(ngModel)]="larvicidesInformation.BsG_gramas"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>Depósito</ion-label>\n      <ion-input type="number"  [(ngModel)]="larvicidesInformation.BsG_depositos"></ion-input>\n    </ion-item>\n  </ion-list>\n\n  <br>\n  <br>\n\n  <!-- Botões de voltar e avançar -->\n  <div text-center>\n\n    <!--Finalizando formulario -->\n    <button ion-button icon-right (click)="Finish()">\n      Finalizar\n      <ion-icon name="exit"></ion-icon>\n    </button>\n  </div>\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\larvicides\larvicides.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]])
    ], LarvicidesPage);
    return LarvicidesPage;
}());

//# sourceMappingURL=larvicides.js.map

/***/ }),

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_api__ = __webpack_require__(97);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginPage = /** @class */ (function () {
    //---------------------------------------------------
    //    Construtor
    //---------------------------------------------------
    function LoginPage(navCtrl, navParams, apiProvider, loading, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiProvider = apiProvider;
        this.loading = loading;
        this.toastCtrl = toastCtrl;
        //---------------------------------------------------
        //    Atributos
        //---------------------------------------------------
        //Dados do Usuario
        this.dados = {};
    }
    //---------------------------------------------------
    //    Métodos
    //---------------------------------------------------
    // Informa via console que estamos na tela de login.
    LoginPage.prototype.ionViewDidLoad = function () {
    };
    // Tentativa de autenticação do Usuário(Login).
    LoginPage.prototype.tryToAuthenticate = function () {
        var _this = this;
        this.showLoader();
        console.log("Dados:  ");
        console.log(this.dados['username'] + "   " + this.dados['senha']);
        // Fazendo a autenticação.
        this.apiProvider.sendAuthenticationData(this.dados['username'], this.dados['senha']).subscribe(function (data) {
            console.log(data);
            _this.loadingMsg.dismiss();
            _this.showMessage('Login Realizado com Sucesso', 2500);
            ;
            // Coleta dados enviados pelo Back-end.
            var response = data;
            var objeto_JSON = JSON.parse(response._body);
            //Token de Autenticação
            _this.apiProvider.setAuthenticationToken(objeto_JSON.auth_token).then(function (result) { console.log("AuthToken Armazenado: " + result); });
            //Preenchimento de dados relacionados ao Agente de Saude vindos do JSON
            _this.dados.nome = objeto_JSON.nome;
            _this.dados.profissao = objeto_JSON.profissao;
            _this.dados.posto = objeto_JSON.posto;
            //Em caso de sucesso, vá para a pagina de Perfil
            _this.GotoTabsPage();
        }
        // Caso contrário, tentar novamente.
        , function (error) {
            console.log('Login Invalido');
            console.log(error);
            _this.showMessage('Login Inválido', 2500);
            return;
        });
    };
    // Abre a página de perfil e passa os parâmetros.
    LoginPage.prototype.GotoTabsPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {
            'nome': this.dados.nome,
            'profissao': this.dados.profissao,
            'posto': this.dados.posto,
        });
    };
    //Função para Desabilitar Autenticação
    LoginPage.prototype.GotoTabsPageNoAuthentication = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], {
            'nome': "Luciana Silva",
            'profissao': "Agente de Saúde",
            'posto': "Posto de Saúde da Várzea",
        });
    };
    LoginPage.prototype.showLoader = function () {
        this.loadingMsg = this.loading.create({
            content: 'Aguarde...'
        });
    };
    LoginPage.prototype.showMessage = function (message, duration) {
        var mess = this.toastCtrl.create({
            message: message,
            duration: duration,
            position: 'bottom'
        });
        mess.present();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\login\login.html"*/'<ion-content padding class="display-settings" no-bouce>     <!-- "no-bouce" indica que a tela não tem rolagem. -->\n\n  <!-- Título do aplicativo. -->\n  <div class="title_app">\n    Geo-Saúde\n  </div>\n\n  <div class="login-content">\n    <form (ngSubmit)="GotoTabsPageNoAuthentication()" class="list-form" ion-fixed>\n      <ion-row justify-content-center align-items-center style="height: 100%">\n\n        <ion-card class="cards-cfg">\n          <!-- Item para o nome do usuário. -->\n          <ion-item>\n            <!-- <ion-label floating style="color: #cedeed; font-weight: bold; font-size: 15px">Usuário</ion-label> -->\n            <ion-input type="text" [(ngModel)]="dados.username" name="username" placeholder="Nome do Usuário" class="input-cfg"></ion-input>\n          </ion-item>\n        </ion-card>\n\n        <ion-card class="cards-cfg">\n          <!-- Item para a senha do usuário -->\n          <ion-item>\n            <!-- <ion-label floating style="color: #cedeed; font-weight: bold; font-size: 15px">Senha</ion-label> -->\n            <ion-input type="password" [(ngModel)]="dados.senha" name="senha" placeholder="Senha" class="input-cfg"></ion-input>\n          </ion-item>\n        </ion-card>\n\n      </ion-row>\n\n      <!-- Botão para realizar o login. -->\n\n      <div id="button">\n        <button ion-button icon-right type="submit" outline color="light" class="button-settings">\n          Entrar\n          <ion-icon name="log-in"></ion-icon>\n        </button>\n        \n        <!--\n        <p text-center ion-text color="light">Ou entre com:</p>\n        -->\n\n\n        <!-- Botões para logar por um meio alternativo. -->\n\n        <!--\n        <ion-grid>\n          <ion-row justify-content-center>\n            <ion-col> \n              <button ion-button icon-only round class="btn-gplus" >\n                <ion-icon name="logo-googleplus" color="light"></ion-icon>\n              </button>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n\n      -->\n      </div>\n    </form>\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\login\login.html"*/,
            providers: [
                __WEBPACK_IMPORTED_MODULE_3__providers_api_api__["a" /* ApiProvider */]
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 134:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PerfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(310);
/*
*   Geo-Saúde: Tela de Perfil
*   Desenvolvedores: Wellington Pinheiro, Anderson Felix, Gabriel Gadelha, Leydson Barros e Ana Carolina.
*
*/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PerfilPage = /** @class */ (function () {
    function PerfilPage(navCtrl, navParams, formBuilder, camera) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.camera = camera;
        this.usuario = {};
        // Inicializa o objeto que guardará a imagem.
        this.form = formBuilder.group({
            image: ['']
        });
    }
    PerfilPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PerfilPage');
    };
    // Recebendo os parâmetros.
    PerfilPage.prototype.ionViewWillEnter = function () {
        this.usuario['nome'] = this.navParams.get('nome');
        this.usuario['profissao'] = this.navParams.get('profissao');
        this.usuario['posto'] = this.navParams.get('posto');
    };
    // Método para obter a imagem.
    PerfilPage.prototype.getPicture = function () {
        var _this = this;
        if (__WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */]['installed']()) {
            this.camera.getPicture({
                destinationType: this.camera.DestinationType.DATA_URL,
                targetWidth: 96,
                targetHeight: 96
            }).then(function (data) {
                _this.form.patchValue({ 'image': 'data:image/jpg;base64, ' + data });
            }, function (err) {
                alert('Unable to take photo!');
            });
        }
        else {
            this.fileInput.nativeElement.click();
        }
    };
    // Carregando a imagem.
    PerfilPage.prototype.processWebImage = function (event) {
        var _this = this;
        var reader = new FileReader();
        reader.onload = function (readerEvent) {
            var imageData = readerEvent.target.result;
            _this.form.patchValue({ 'image': imageData });
        };
        reader.readAsDataURL(event.target.files[0]);
    };
    // Carregando o estilo da imagem.
    PerfilPage.prototype.getProfileImageStyle = function () {
        return 'url(' + this.form.controls['image'].value + ')';
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])('fileInput'),
        __metadata("design:type", Object)
    ], PerfilPage.prototype, "fileInput", void 0);
    PerfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-perfil',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\perfil\perfil.html"*/'<!-- Início do Header -->\n\n<ion-header>\n\n  <ion-navbar color="primary">\n\n    <!-- Botão para o side menu -->\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n\n    <!-- Título da tela. -->\n    <ion-title class="title_page">Perfil</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<!-- Final do Header -->\n\n\n<!-- Início do Ion Content -->\n\n<ion-content>\n\n  <form>\n\n    <div class="upperDiv">\n      \n      <!-- Exibindo a imagem do usuário. -->\n     <!-- <div align="center"> -->\n\n      <input type="file" #fileInput style="visibility: hidden; height: 40px" name="files[]" (change)="processWebImage($event)"/>\n\n\n      <div class="profile-image-wrapper" (click)="getPicture()">\n        <div class="profile-image-placeholder" *ngIf="!this.form.controls.image.value">\n          <ion-icon name="ios-camera-outline" class="ios-camera-outline" md="ios-camera-outline" style="color: white !important; opacity: 0.6">\n          </ion-icon>\n        </div>\n\n        <div class="profile-image" [style.backgroundImage]="getProfileImageStyle()" *ngIf="this.form.controls.image.value">\n        </div>\n\n        <!--\n        <ion-avatar class="avatar" item-start>\n          <img [style.border]="\'5px solid\'" src="./assets/imgs/img_agente_1.jpg">\n        </ion-avatar>-->\n        \n\n      </div>\n\n      <!-- Exibindo as informações sobre o usuário. -->\n\n\n      <h3 class="agent_name">{{usuario.nome}}</h3>\n\n      <h3 class="agent_profession">{{usuario.profissao}}</h3>\n\n      <h3 class="agent_locality">{{usuario.posto}}</h3>\n\n\n    </div>\n\n    <!-- Exibindo as medalhas do usuário. -->\n    <ion-item>\n\n      <table>\n\n        <tr align="center">\n          <td>\n            <img src="./assets/imgs/medalhas/medal-1.png" width="50%">\n          </td>\n          <td>\n            <img src="./assets//imgs/medalhas/medal-2.png" width="50%">\n          </td>\n          <td>\n            <img src="./assets/imgs/medalhas/medal-3.png" width="50%">\n          </td>\n          <td>\n            <img src="./assets/imgs/medalhas/medal-4.png" width="50%">\n          </td>\n        </tr>\n\n      </table>\n\n    </ion-item>\n\n  </form>\n\n</ion-content>\n\n<!-- Final do Ion Content -->'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\perfil\perfil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */]])
    ], PerfilPage);
    return PerfilPage;
}());

//# sourceMappingURL=perfil.js.map

/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__map_modal_map_modal__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_models_controllers_generic_controller__ = __webpack_require__(453);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MapPage = /** @class */ (function (_super) {
    __extends(MapPage, _super);
    function MapPage(navCtrl, navParams, modalCtrl) {
        var _this = _super.call(this) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.modalCtrl = modalCtrl;
        _this.geocoder = new google.maps.Geocoder;
        return _this;
    }
    MapPage.prototype.openModal = function (latLng) {
        var mctrl = this.modalCtrl;
        this.geocodeLatLng(latLng, function (addr) {
            var myModal = mctrl.create(__WEBPACK_IMPORTED_MODULE_2__map_modal_map_modal__["a" /* MapModalPage */], { address: addr });
            myModal.present();
        });
    };
    MapPage.prototype.geocodeLatLng = function (input, callback) {
        var latlngStr = input.split(',', 2);
        var latlng = { lat: parseFloat(latlngStr[0].slice(1)), lng: parseFloat(latlngStr[1]) };
        var result = this.geocoder.geocode({ 'location': latlng }, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    callback(results[0].formatted_address);
                }
                else {
                    console.log('No results found');
                }
            }
            else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
    };
    MapPage.prototype.ionViewDidLoad = function () {
        this.showMap();
    };
    MapPage.prototype.showMap = function () {
        // localização - latitude e longitude
        var location = new google.maps.LatLng(-8.05362071, -34.95265961);
        var options = {
            center: location,
            zoom: 16,
            streetViewControl: true,
            mapTypeId: 'terrain' // 'satellite' / 'hybrid
        };
        var map = new google.maps.Map(this.mapRef.nativeElement, options);
        var _loop_1 = function (place) {
            var g_location = new google.maps.LatLng(place[0], place[1]);
            var visited = place[2];
            var marker = this_1.addMarker(g_location, map, visited);
            var self_1 = this_1;
            marker.addListener('click', function (ev) {
                self_1.openModal(marker.getPosition().toString());
            });
        };
        var this_1 = this;
        for (var _i = 0, _a = this.getCurrentUser().schedule; _i < _a.length; _i++) {
            var place = _a[_i];
            _loop_1(place);
        }
    };
    MapPage.prototype.addMarker = function (position, map, visited) {
        var icon = 'http://maps.google.com/mapfiles/ms/icons/red.png';
        if (visited) {
            icon = 'http://maps.google.com/mapfiles/ms/icons/blue.png';
        }
        return new google.maps.Marker({
            position: position,
            map: map,
            icon: icon
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], MapPage.prototype, "mapRef", void 0);
    MapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-map',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\map\map.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n\n    <!-- Botão para o side menu -->\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n\n    <ion-title>\n      Mapa\n    </ion-title>\n\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-content>\n  <div #map id="map"></div>\n</ion-content>'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\map\map.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], MapPage);
    return MapPage;
}(__WEBPACK_IMPORTED_MODULE_3__app_models_controllers_generic_controller__["a" /* GenericController */]));

//# sourceMappingURL=map.js.map

/***/ }),

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/*          MapModalPage
 *
 *  Está página exibe a tela para atualização das informações do imóvel.
*/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MapModalPage = /** @class */ (function () {
    function MapModalPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        // Obtém parâmetro passado.
        this.address = this.navParams.get('address');
    }
    MapModalPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss();
    };
    MapModalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MapModalPage');
    };
    MapModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-map-modal',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\map-modal\map-modal.html"*/'<!--\n  Generated template for the MapModalPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{address}}</ion-title>\n    <ion-buttons end>\n      <button ion-button block (click)="closeModal()">Fechar</button>\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n      <ion-list>\n      <ion-item>\n        <ion-label>Tipo de Imovel</ion-label>\n        <ion-select [(ngModel)]="TipoDeImovel">\n          <ion-option value="res">Residência</ion-option>\n          <ion-option value="com">Comercio</ion-option>\n          <ion-option value="ind">Indústria</ion-option>\n          <ion-option value="SerP">Serviço Privado</ion-option>\n          <ion-option value="resC">Residência Com</ion-option>\n          <ion-option value="imovCon">Imovel em Construção</ion-option>\n          <ion-option value="terBal">Terreno Baldio</ion-option>\n          <ion-option value="ponEs">Ponto Estratégico</ion-option>\n          <ion-option value="out">Outros</ion-option>\n        </ion-select>\n      </ion-item>\n    </ion-list>\n\n    <ion-list>\n      <ion-item>\n        <ion-label>Situação</ion-label>\n        <ion-select [(ngModel)]="situacao">\n          <ion-option value="insp">Inspecionado</ion-option>\n          <ion-option value="inspN">Não Inspecionado</ion-option>\n          <ion-option value="recu">Recuperado</ion-option>\n        </ion-select>\n      </ion-item>\n    </ion-list>\n\n    <ion-item>\n      <ion-label>Data</ion-label>\n      <ion-datetime displayFormat="MMM DD YYYY" [(ngModel)]="myDate"></ion-datetime>\n    </ion-item>\n\n    <button ion-button color="secondary" round full (click)= "closeModal()">Atualizar Situação</button>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\map-modal\map-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
    ], MapModalPage);
    return MapModalPage;
}());

//# sourceMappingURL=map-modal.js.map

/***/ }),

/***/ 145:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 145;

/***/ }),

/***/ 187:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about/about.module": [
		476,
		15
	],
	"../pages/achievements/achievements.module": [
		477,
		14
	],
	"../pages/activities/activities.module": [
		479,
		13
	],
	"../pages/aedes/aedes.module": [
		478,
		12
	],
	"../pages/culex/culex.module": [
		480,
		11
	],
	"../pages/immobile/immobile.module": [
		482,
		10
	],
	"../pages/larvicides/larvicides.module": [
		481,
		9
	],
	"../pages/login/login.module": [
		483,
		8
	],
	"../pages/map-modal/map-modal.module": [
		484,
		7
	],
	"../pages/map/map.module": [
		485,
		6
	],
	"../pages/perfil/perfil.module": [
		486,
		5
	],
	"../pages/select-form/select-form.module": [
		487,
		4
	],
	"../pages/start-forms/start-forms.module": [
		488,
		3
	],
	"../pages/start-visit/start-visit.module": [
		489,
		2
	],
	"../pages/trash/trash.module": [
		490,
		1
	],
	"../pages/tratamento-psa/tratamento-psa.module": [
		491,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 187;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__perfil_perfil__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__achievements_achievements__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__activities_activities__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__map_map__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TabsPage = /** @class */ (function () {
    function TabsPage(navParams) {
        this.navParams = navParams;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_1__perfil_perfil__["a" /* PerfilPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__achievements_achievements__["a" /* AchievementsPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__activities_activities__["a" /* ActivitiesPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_4__map_map__["a" /* MapPage */];
        this.navParams = navParams;
        this.loginData = this.navParams.data;
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\tabs\tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" [rootParams]="loginData" tabTitle="Perfil" tabIcon="md-contact" ></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Conquistas" tabIcon="md-trophy"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Atividades" tabIcon="ios-list-box-outline"></ion-tab>\n  <ion-tab [root]="tab4Root" tabTitle="Mapa" tabIcon="ios-map"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["k" /* NavParams */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return VisitInformationPSA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return Immobile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return Trash; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return VtAedes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BreendingGroundsAedes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return Treatment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return VtCulex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return BreendingGroundsCulex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return TpLarvicides; });
// VisitInformationPSA contém a estrutura que será utilizada para armazenar informações no storage.
var VisitInformationPSA = /** @class */ (function () {
    function VisitInformationPSA(places, // Logradouro
        immobile, // Imóvel
        trash, // Lixo
        vtAedes, // Vetor aedes
        vtCulex, // Vetor cúlex
        ovitraps, // Ovitrampas
        larvicides) {
        this.places = places;
        this.immobile = immobile;
        this.trash = trash;
        this.vtAedes = vtAedes;
        this.vtCulex = vtCulex;
        this.ovitraps = ovitraps;
        this.larvicides = larvicides;
    }
    /**
     * setPlace
     */
    VisitInformationPSA.prototype.setPlace = function (place) {
        this.places = place;
    };
    /**
     * setImmobile
     */
    VisitInformationPSA.prototype.setImmobile = function (immobile) {
        this.immobile = immobile;
    };
    /**
     * setTrash
     */
    VisitInformationPSA.prototype.setTrash = function (trash) {
        this.trash = trash;
    };
    /**
     * setVtAedes
     */
    VisitInformationPSA.prototype.setVtAedes = function (vtAedes) {
        this.vtAedes = vtAedes;
    };
    /**
     * setVtCulex
     */
    VisitInformationPSA.prototype.setVtCulex = function (vtCulex) {
        this.vtCulex = vtCulex;
    };
    /**
     * setOvitraps
     */
    VisitInformationPSA.prototype.setOvitraps = function (ovitraps) {
        this.ovitraps = ovitraps;
    };
    /**
     * setLarvicides
     */
    VisitInformationPSA.prototype.setLarvicides = function (larvicides) {
        this.larvicides = larvicides;
    };
    return VisitInformationPSA;
}());

// -------------------------------------------------------------------------------------
var Immobile = /** @class */ (function () {
    function Immobile() {
    }
    return Immobile;
}());

var Trash = /** @class */ (function () {
    function Trash() {
    }
    return Trash;
}());

var VtAedes = /** @class */ (function () {
    function VtAedes() {
    }
    return VtAedes;
}());

var BreendingGroundsAedes = /** @class */ (function () {
    function BreendingGroundsAedes() {
    }
    return BreendingGroundsAedes;
}());

var Treatment = /** @class */ (function () {
    function Treatment() {
    }
    return Treatment;
}());

var VtCulex = /** @class */ (function () {
    function VtCulex() {
    }
    return VtCulex;
}());

var BreendingGroundsCulex = /** @class */ (function () {
    function BreendingGroundsCulex() {
        this.fosse = false; // Fossa
        this.cistern = false; // Cisterna
        this.channelDichGutters = false; // Canal-Canaletas-Valas
        this.puddle = false; // Charco, poça de água
        this.inspectionBoxDrainage = false; // Caixa de inspeção - Drenagem
    }
    return BreendingGroundsCulex;
}());

var TpLarvicides = /** @class */ (function () {
    function TpLarvicides() {
    }
    return TpLarvicides;
}());

//# sourceMappingURL=VisitInformationPsa.js.map

/***/ }),

/***/ 353:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(371);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 371:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser_animations__ = __webpack_require__(471);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_tooltips__ = __webpack_require__(473);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(475);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_perfil_perfil__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_achievements_achievements__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_map_map__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_immobile_immobile__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_activities_activities__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__ = __webpack_require__(351);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_map_modal_map_modal__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_about_about__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_aedes_aedes__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_culex_culex__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_larvicides_larvicides__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_tratamento_psa_tratamento_psa__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_select_form_select_form__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_start_forms_start_forms__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_trash_trash__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_start_visit_start_visit__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__angular_common__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_api_api__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__providers_psa_form_psa_form__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_liraa_form_liraa_form__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__providers_network_network__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__providers_api_send_forms_api_send_forms__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_camera__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__ionic_native_network__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__angular_http__ = __webpack_require__(98);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






// ------------- Importando Pages -----------------------------------------------------------




















// ------------- Importando Providers -----------------------------------------------------------





// ------------- Importando Plugins -----------------------------------------------------------




var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_perfil_perfil__["a" /* PerfilPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_achievements_achievements__["a" /* AchievementsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_map_map__["a" /* MapPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_immobile_immobile__["a" /* ImmobilePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_map_modal_map_modal__["a" /* MapModalPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_aedes_aedes__["a" /* AedesPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_culex_culex__["a" /* CulexPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_larvicides_larvicides__["a" /* LarvicidesPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_tratamento_psa_tratamento_psa__["a" /* TratamentoPsaPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_select_form_select_form__["a" /* SelectFormPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_start_forms_start_forms__["a" /* StartFormsPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_trash_trash__["a" /* TrashPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_activities_activities__["a" /* ActivitiesPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_start_visit_start_visit__["a" /* StartVisitPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_4_ionic_tooltips__["a" /* TooltipsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/about/about.module#AboutPageModule', name: 'AboutPage', segment: 'about', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/achievements/achievements.module#AchievementsPageModule', name: 'AchievementsPage', segment: 'achievements', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/aedes/aedes.module#AedesPageModule', name: 'AedesPage', segment: 'aedes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/activities/activities.module#ActivitiesPageModule', name: 'ActivitiesPage', segment: 'activities', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/culex/culex.module#CulexPageModule', name: 'CulexPage', segment: 'culex', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/larvicides/larvicides.module#LarvicidesPageModule', name: 'LarvicidesPage', segment: 'larvicides', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/immobile/immobile.module#ImmobilePageModule', name: 'ImmobilePage', segment: 'immobile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/map-modal/map-modal.module#MapModalPageModule', name: 'MapModalPage', segment: 'map-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/map/map.module#MapPageModule', name: 'MapPage', segment: 'map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/perfil/perfil.module#PerfilPageModule', name: 'PerfilPage', segment: 'perfil', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/select-form/select-form.module#SelectFormPageModule', name: 'SelectFormPage', segment: 'select-form', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/start-forms/start-forms.module#StartFormsPageModule', name: 'StartFormsPage', segment: 'start-forms', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/start-visit/start-visit.module#StartVisitPageModule', name: 'StartVisitPage', segment: 'start-visit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trash/trash.module#TrashPageModule', name: 'TrashPage', segment: 'trash', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tratamento-psa/tratamento-psa.module#TratamentoPsaPageModule', name: 'TratamentoPsaPage', segment: 'tratamento-psa', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_34__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_33__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_perfil_perfil__["a" /* PerfilPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_achievements_achievements__["a" /* AchievementsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_map_map__["a" /* MapPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_immobile_immobile__["a" /* ImmobilePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_map_modal_map_modal__["a" /* MapModalPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_aedes_aedes__["a" /* AedesPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_culex_culex__["a" /* CulexPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_larvicides_larvicides__["a" /* LarvicidesPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_tratamento_psa_tratamento_psa__["a" /* TratamentoPsaPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_select_form_select_form__["a" /* SelectFormPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_start_forms_start_forms__["a" /* StartFormsPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_trash_trash__["a" /* TrashPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_activities_activities__["a" /* ActivitiesPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_start_visit_start_visit__["a" /* StartVisitPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_26__providers_api_api__["a" /* ApiProvider */],
                __WEBPACK_IMPORTED_MODULE_27__providers_psa_form_psa_form__["a" /* PsaFormProvider */],
                __WEBPACK_IMPORTED_MODULE_25__angular_common__["d" /* DatePipe */],
                __WEBPACK_IMPORTED_MODULE_28__providers_liraa_form_liraa_form__["a" /* LiraaFormProvider */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_30__providers_api_send_forms_api_send_forms__["a" /* ApiSendFormsProvider */],
                __WEBPACK_IMPORTED_MODULE_32__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_29__providers_network_network__["a" /* NetworkProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 424:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 193,
	"./af.js": 193,
	"./ar": 194,
	"./ar-dz": 195,
	"./ar-dz.js": 195,
	"./ar-kw": 196,
	"./ar-kw.js": 196,
	"./ar-ly": 197,
	"./ar-ly.js": 197,
	"./ar-ma": 198,
	"./ar-ma.js": 198,
	"./ar-sa": 199,
	"./ar-sa.js": 199,
	"./ar-tn": 200,
	"./ar-tn.js": 200,
	"./ar.js": 194,
	"./az": 201,
	"./az.js": 201,
	"./be": 202,
	"./be.js": 202,
	"./bg": 203,
	"./bg.js": 203,
	"./bn": 204,
	"./bn.js": 204,
	"./bo": 205,
	"./bo.js": 205,
	"./br": 206,
	"./br.js": 206,
	"./bs": 207,
	"./bs.js": 207,
	"./ca": 208,
	"./ca.js": 208,
	"./cs": 209,
	"./cs.js": 209,
	"./cv": 210,
	"./cv.js": 210,
	"./cy": 211,
	"./cy.js": 211,
	"./da": 212,
	"./da.js": 212,
	"./de": 213,
	"./de-at": 214,
	"./de-at.js": 214,
	"./de-ch": 215,
	"./de-ch.js": 215,
	"./de.js": 213,
	"./dv": 216,
	"./dv.js": 216,
	"./el": 217,
	"./el.js": 217,
	"./en-au": 218,
	"./en-au.js": 218,
	"./en-ca": 219,
	"./en-ca.js": 219,
	"./en-gb": 220,
	"./en-gb.js": 220,
	"./en-ie": 221,
	"./en-ie.js": 221,
	"./en-nz": 222,
	"./en-nz.js": 222,
	"./eo": 223,
	"./eo.js": 223,
	"./es": 224,
	"./es-do": 225,
	"./es-do.js": 225,
	"./es.js": 224,
	"./et": 226,
	"./et.js": 226,
	"./eu": 227,
	"./eu.js": 227,
	"./fa": 228,
	"./fa.js": 228,
	"./fi": 229,
	"./fi.js": 229,
	"./fo": 230,
	"./fo.js": 230,
	"./fr": 231,
	"./fr-ca": 232,
	"./fr-ca.js": 232,
	"./fr-ch": 233,
	"./fr-ch.js": 233,
	"./fr.js": 231,
	"./fy": 234,
	"./fy.js": 234,
	"./gd": 235,
	"./gd.js": 235,
	"./gl": 236,
	"./gl.js": 236,
	"./gom-latn": 237,
	"./gom-latn.js": 237,
	"./he": 238,
	"./he.js": 238,
	"./hi": 239,
	"./hi.js": 239,
	"./hr": 240,
	"./hr.js": 240,
	"./hu": 241,
	"./hu.js": 241,
	"./hy-am": 242,
	"./hy-am.js": 242,
	"./id": 243,
	"./id.js": 243,
	"./is": 244,
	"./is.js": 244,
	"./it": 245,
	"./it.js": 245,
	"./ja": 246,
	"./ja.js": 246,
	"./jv": 247,
	"./jv.js": 247,
	"./ka": 248,
	"./ka.js": 248,
	"./kk": 249,
	"./kk.js": 249,
	"./km": 250,
	"./km.js": 250,
	"./kn": 251,
	"./kn.js": 251,
	"./ko": 252,
	"./ko.js": 252,
	"./ky": 253,
	"./ky.js": 253,
	"./lb": 254,
	"./lb.js": 254,
	"./lo": 255,
	"./lo.js": 255,
	"./lt": 256,
	"./lt.js": 256,
	"./lv": 257,
	"./lv.js": 257,
	"./me": 258,
	"./me.js": 258,
	"./mi": 259,
	"./mi.js": 259,
	"./mk": 260,
	"./mk.js": 260,
	"./ml": 261,
	"./ml.js": 261,
	"./mr": 262,
	"./mr.js": 262,
	"./ms": 263,
	"./ms-my": 264,
	"./ms-my.js": 264,
	"./ms.js": 263,
	"./my": 265,
	"./my.js": 265,
	"./nb": 266,
	"./nb.js": 266,
	"./ne": 267,
	"./ne.js": 267,
	"./nl": 268,
	"./nl-be": 269,
	"./nl-be.js": 269,
	"./nl.js": 268,
	"./nn": 270,
	"./nn.js": 270,
	"./pa-in": 271,
	"./pa-in.js": 271,
	"./pl": 272,
	"./pl.js": 272,
	"./pt": 273,
	"./pt-br": 274,
	"./pt-br.js": 274,
	"./pt.js": 273,
	"./ro": 275,
	"./ro.js": 275,
	"./ru": 276,
	"./ru.js": 276,
	"./sd": 277,
	"./sd.js": 277,
	"./se": 278,
	"./se.js": 278,
	"./si": 279,
	"./si.js": 279,
	"./sk": 280,
	"./sk.js": 280,
	"./sl": 281,
	"./sl.js": 281,
	"./sq": 282,
	"./sq.js": 282,
	"./sr": 283,
	"./sr-cyrl": 284,
	"./sr-cyrl.js": 284,
	"./sr.js": 283,
	"./ss": 285,
	"./ss.js": 285,
	"./sv": 286,
	"./sv.js": 286,
	"./sw": 287,
	"./sw.js": 287,
	"./ta": 288,
	"./ta.js": 288,
	"./te": 289,
	"./te.js": 289,
	"./tet": 290,
	"./tet.js": 290,
	"./th": 291,
	"./th.js": 291,
	"./tl-ph": 292,
	"./tl-ph.js": 292,
	"./tlh": 293,
	"./tlh.js": 293,
	"./tr": 294,
	"./tr.js": 294,
	"./tzl": 295,
	"./tzl.js": 295,
	"./tzm": 296,
	"./tzm-latn": 297,
	"./tzm-latn.js": 297,
	"./tzm.js": 296,
	"./uk": 298,
	"./uk.js": 298,
	"./ur": 299,
	"./ur.js": 299,
	"./uz": 300,
	"./uz-latn": 301,
	"./uz-latn.js": 301,
	"./uz.js": 300,
	"./vi": 302,
	"./vi.js": 302,
	"./x-pseudo": 303,
	"./x-pseudo.js": 303,
	"./yo": 304,
	"./yo.js": 304,
	"./zh-cn": 305,
	"./zh-cn.js": 305,
	"./zh-hk": 306,
	"./zh-hk.js": 306,
	"./zh-tw": 307,
	"./zh-tw.js": 307
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 424;

/***/ }),

/***/ 453:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GenericController; });
var GenericController = /** @class */ (function () {
    function GenericController() {
    }
    GenericController.prototype.getCurrentUser = function () {
        return {
            name: 'nome',
            schedule: [[-8.05362071, -34.95265961, true], [-8.05352071, -34.95265961, false], [-8.05361071, -34.95264961, true]]
        };
    };
    return GenericController;
}());

//# sourceMappingURL=generic-controller.js.map

/***/ }),

/***/ 475:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(351);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_about_about__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_network_network__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    // -------------- Construtor -----------------------------
    function MyApp(platform, statusBar, splashScreen, networkProvider) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.networkProvider = networkProvider;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        this.newFormsToSend = false;
        this.initializeApp();
        this.pages = [
            { title: 'Sobre', component: __WEBPACK_IMPORTED_MODULE_5__pages_about_about__["a" /* AboutPage */] }
        ];
    }
    // -------------- Métodos -----------------------------
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.networkProvider.initializeNetworkEvents(); // Inicia os eventos para monitoramento da conexão com a internet.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // this.nav.setRoot(page.component);
    };
    // Alterar o estado da variável newFormsToSend.
    MyApp.prototype.setNewFormsToSend = function (val) {
        this.newFormsToSend = val;
    };
    // Informar o valor da variável newFormsToSend.
    MyApp.prototype.getNewFormsToSend = function () {
        return this.newFormsToSend;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\app\app.html"*/'<!-- Definindo o menu -->\n\n<ion-menu [content]="content">\n\n    <!-- Título do menu -->\n\n    <ion-header>\n        <ion-toolbar class="toolbar-setting">\n            <h2 ion-text class="title-menu">\n                Geo-Saúde\n            </h2>\n        </ion-toolbar>\n    </ion-header>\n\n    <!-- Lista que aparecerá quando o menu for aberto -->\n\n    <ion-content>\n        <ion-list>\n            <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n                {{p.title}}\n            </button>\n        </ion-list>\n    </ion-content>\n\n</ion-menu>\n\n<ion-nav [root]="rootPage" #content></ion-nav>'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_6__providers_network_network__["a" /* NetworkProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PsaFormProvider; });
/* unused harmony export VisitInformationPSAList */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

//import { Http } from '@angular/http';




var PsaFormProvider = /** @class */ (function () {
    // ----------------- Construtor ------------------------------------------------------------
    function PsaFormProvider(datepipe, eventsCtrl) {
        this.datepipe = datepipe;
        this.eventsCtrl = eventsCtrl;
        // Criando novo storage.
        this.psaFormDb = new __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]({
            name: '__psadb',
            storeName: '__psaForm',
            driverOrder: ['indexeddb', 'sqlite', 'websql ']
        });
    }
    // ----------------- Métodos ------------------------------------------------------------
    PsaFormProvider.prototype.insert = function (visitInformationPSA) {
        // Gerando a chave de armazenamento.
        var key = this.datepipe.transform(new Date(), "ddMMyyyyHHmmss");
        return this.save(key, visitInformationPSA);
    };
    PsaFormProvider.prototype.update = function (key, visitInformationPsa) {
        return this.save(key, visitInformationPsa);
    };
    PsaFormProvider.prototype.save = function (key, visitInformationPSA) {
        return this.psaFormDb.set(key, visitInformationPSA);
    };
    PsaFormProvider.prototype.remove = function (key) {
        return this.psaFormDb.remove(key);
    };
    PsaFormProvider.prototype.getAll = function () {
        var _this = this;
        var visitsInformations = []; // Declarando a lista que guardará as informações que serão retornadas
        // Lendo cada linha da estrutura do storage.
        return this.psaFormDb.forEach(function (value, key, iterationNumber) {
            // Publica um evento para avisar que há dados no storage que precisam ser enviados ao servidor.
            _this.eventsCtrl.publish('haveNewFormsToSend', true);
            // Declarando objeto que receberá o valor dos parâmetros de cada linha.
            var visit = new VisitInformationPSAList(null, null);
            visit.setKey(key); // Recebe a chave.
            visit.setVisitInformation(value); // Recebe o valor, neste caso é o objeto VisitInformationPSA.
            visitsInformations.push(visit); // Coloca objeto com as informações das visitas na lista.
        })
            .then(function () {
            return Promise.resolve(visitsInformations);
        })
            .catch(function (error) {
            console.log(error);
            return Promise.reject(error);
        });
    };
    PsaFormProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_common__["d" /* DatePipe */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* Events */]])
    ], PsaFormProvider);
    return PsaFormProvider;
}());

// Está classe será utilizada quando for necessário recuperar o que está armazenado no storage.
var VisitInformationPSAList = /** @class */ (function () {
    function VisitInformationPSAList(key, visitInformationPSA) {
        this.key = key;
        this.visitInformationPSA = visitInformationPSA;
    }
    /**
     * setKey
     */
    VisitInformationPSAList.prototype.setKey = function (key) {
        this.key = key;
    };
    /**
     * setVisitInformarionPSA
     */
    VisitInformationPSAList.prototype.setVisitInformation = function (visit) {
        this.visitInformationPSA = visit;
    };
    return VisitInformationPSAList;
}());

//# sourceMappingURL=psa-form.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LiraaFormProvider; });
/* unused harmony export VisitInformationLIRAaList */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

//import { Http } from '@angular/http';




var LiraaFormProvider = /** @class */ (function () {
    // -------------- Construtor -----------------------------
    function LiraaFormProvider(datepipe, eventsCtrl) {
        this.datepipe = datepipe;
        this.eventsCtrl = eventsCtrl;
        // Criando novo banco local.
        this.liraaFormDb = new __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]({
            name: '__liraadb',
            storeName: '__liraaForm',
            driverOrder: ['indexeddb', 'sqlite', 'websql ']
        });
    }
    // -------------- Métodos --------------------------
    // Insere novo dados no banco local.
    LiraaFormProvider.prototype.insert = function (visitInformationLIRAa) {
        // Gerando a chave de armazenamento.
        var key = this.datepipe.transform(new Date(), "ddMMyyyyHHmmss");
        return this.save(key, visitInformationLIRAa);
    };
    // Atualiza dados do banco local.
    LiraaFormProvider.prototype.update = function (key, visitInformationLIRAa) {
        return this.save(key, visitInformationLIRAa);
    };
    // Salva dados no banco local.
    LiraaFormProvider.prototype.save = function (key, visitInformationLIRAa) {
        return this.liraaFormDb.set(key, visitInformationLIRAa);
    };
    // Deleta dados do banco local.
    LiraaFormProvider.prototype.remove = function (key) {
        return this.liraaFormDb.remove(key);
    };
    // Busca todos os dados que estão no banco local.
    LiraaFormProvider.prototype.getAll = function () {
        var _this = this;
        // Declarando a lista que guardará as informações que serão retornadas
        var visitsInformations = [];
        // Lendo cada linha da estrutura do storage.
        return this.liraaFormDb.forEach(function (value, key, iterationNumber) {
            // Publica um evento caso avisando que há dados para ser enviados ao servidor.
            _this.eventsCtrl.publish('haveNewFormsToSend', true);
            // Declarando objeto que receberá o valor dos parâmetros de cada linha.
            var visit = new VisitInformationLIRAaList(null, null);
            visit.setKey(key); // Recebe a chave.
            visit.setVisitInformation(value); // Recebe o valor, neste caso é o objeto VisitInformationLIRAa.
            visitsInformations.push(visit); // Coloca objeto com as informações das visitas na lista.
        })
            .then(function (msg) {
            // Retorna lista com todos os formulários liraa do banco local.
            return Promise.resolve(visitsInformations);
        })
            .catch(function (error) {
            // Retorna o erro, caso ocorra.
            return Promise.reject(error);
        });
    };
    LiraaFormProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_common__["d" /* DatePipe */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* Events */]])
    ], LiraaFormProvider);
    return LiraaFormProvider;
}());

// Está classe será utilizada quando for necessário recuperar o que está armazenado no storage.
var VisitInformationLIRAaList = /** @class */ (function () {
    function VisitInformationLIRAaList(key, visitInformationLIRAa) {
        this.key = key;
        this.visitInformationLIRAa = visitInformationLIRAa;
    }
    /**
     * setKey
     */
    VisitInformationLIRAaList.prototype.setKey = function (key) {
        this.key = key;
    };
    /**
     * setVisitInformationLIRAa
     */
    VisitInformationLIRAaList.prototype.setVisitInformation = function (visit) {
        this.visitInformationLIRAa = visit;
    };
    return VisitInformationLIRAaList;
}());

//# sourceMappingURL=liraa-form.js.map

/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NetworkProvider = /** @class */ (function () {
    // -------------- Construtor --------------------------
    function NetworkProvider(network, eventCtrl, toast) {
        this.network = network;
        this.eventCtrl = eventCtrl;
        this.toast = toast;
        this.status = "online";
    }
    // -------------- Métodos --------------------------
    // Inicializa evento de rede.
    NetworkProvider.prototype.initializeNetworkEvents = function () {
        var _this = this;
        // Quando a internet for desligada.
        this.network.onDisconnect().subscribe(function () {
            if (_this.status == "online") {
                // Publica evento avisando que a rede está offline.
                _this.eventCtrl.publish('network:offline');
                // Altera estado da rede para offline.
                _this.status = "offline";
                _this.toast.create({
                    message: "Rede desconectada",
                    duration: 5000
                }).present();
            }
        });
        // Quando a internet for ligada.
        this.network.onConnect().subscribe(function () {
            if (_this.status === "offline") {
                if (_this.network.type === 'wifi' || _this.network.type == "3g" || _this.network.type == "4g") {
                    // Publica evento avisando que a rede está online.
                    _this.eventCtrl.publish('network:online');
                    // Altera estado da rede para online.
                    _this.status = "online";
                    _this.toast.create({
                        message: "Rede conectada: " + _this.network.type,
                        duration: 5000
                    }).present();
                }
            }
        });
    };
    // Retorna o estado da rede.
    NetworkProvider.prototype.getNetworkStatus = function () {
        return this.status;
    };
    NetworkProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* ToastController */]])
    ], NetworkProvider);
    return NetworkProvider;
}());

//# sourceMappingURL=network.js.map

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TratamentoPsaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__classes_VisitInformationPsa__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TratamentoPsaPage = /** @class */ (function () {
    // -------------- Métodos ------------------------
    function TratamentoPsaPage(navCtrl, navParams, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        // Instância de tratamento, onde será guardado as informações preenchidas para o tratamento no formulário PSA.
        this.tpTreatment = new __WEBPACK_IMPORTED_MODULE_2__classes_VisitInformationPsa__["f" /* Treatment */]();
        this.visitInformation = this.navParams.get('visitInformation');
        console.log(this.visitInformation);
    }
    TratamentoPsaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TratamentoPsaPage');
    };
    TratamentoPsaPage.prototype.ionViewWillEnter = function () {
        this.focusType = this.navParams.get("foco");
    };
    TratamentoPsaPage.prototype.GoBack = function () {
        this.navCtrl.pop();
    };
    TratamentoPsaPage.prototype.Finish = function () {
        // Retirar as páginas anteriores da pilha e voltar para a StartFormsPage.
        // É verificado qual o tipo de displayName que será passado como parâmetro, de forma a ajudar no distinção de qual objeto armazenar no VisitInformation.
        if (this.focusType == "Aedes") {
            // Ao terminar o preenchimento das informações, será retornado para a página StartForms juntamente com o objeto vtAedesInformation.
            this.vtAedesInformation = new __WEBPACK_IMPORTED_MODULE_2__classes_VisitInformationPsa__["h" /* VtAedes */](); // Instância de um novo objeto vtAedesInformation.
            this.vtAedesInformation.tpBreedingGrounds = this.navParams.get('breendingGroundAedes'); // Atribuindo o parâmetro de tipos de criadouros.
            this.vtAedesInformation.tpTreatment = this.tpTreatment; // Atribuindo as informações sobre o tratamento.
            this.visitInformation.setVtAedes(this.vtAedesInformation);
            console.log(this.visitInformation);
            //this.navCtrl.setRoot(StartFormsPage, { pageFlag: "false", formName: "PSA", visitInformation: this.visitInformation, fieldAedesDone: true });
            this.events.publish('fieldAedesDone', this.visitInformation);
            //this.events.publish('finalizeAedesPage');
        }
        else {
            // Ao terminar o preenchimento das informações, será retornado para a página StartForms juntamente com o objeto vtCulexInformation.
            this.vtCulexInformation = new __WEBPACK_IMPORTED_MODULE_2__classes_VisitInformationPsa__["i" /* VtCulex */](); // Instância de um novo objeto vtCulexInformation.
            this.vtCulexInformation.tpBreedingGrounds = this.navParams.get('breendingGroundCulex'); // Atribuindo o parâmetro de tipos de criadouros.
            this.vtCulexInformation.tpTreatment = this.tpTreatment; // Atribuindo as informações sobre o tratamento.
            this.visitInformation.setVtCulex(this.vtCulexInformation);
            console.log(this.visitInformation);
            //this.navCtrl.setRoot(StartFormsPage, { pageFlag: "false", formName: "PSA", visitInformation: this.visitInformation, fieldCulexDone: true });
            this.events.publish('fieldCulexDone', this.visitInformation);
        }
        //this.navCtrl.popToRoot();
        this.navCtrl.pop();
    };
    TratamentoPsaPage.prototype.ngOnDestroy = function () {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.navCtrl.pop();
    };
    TratamentoPsaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-tratamento-psa',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\tratamento-psa\tratamento-psa.html"*/'<!--\n  HTML da Tela de Tratamento para o PSA\n-->\n\n\n<ion-header>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n  <ion-label text-center>\n    <h1>Tratamento</h1>\n    <h2>Tipos de Tratamento</h2>\n  </ion-label>\n\n\n  <!-- Lista com os tipos de tratamentos -->\n  \n  <ion-list>\n\n\n    <ion-item>\n      <ion-label>Positivo: </ion-label>\n      <ion-input [(ngModel)]="tpTreatment.positive" type="number"></ion-input>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>Mecânico: </ion-label>\n      <ion-input [(ngModel)]="tpTreatment.mechanical" type="number"></ion-input>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>Biológico:</ion-label>\n      <ion-input [(ngModel)]="tpTreatment.biological" type="number"></ion-input>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>Químico:</ion-label>\n      <ion-input [(ngModel)]="tpTreatment.chemical" type="number"></ion-input>\n    </ion-item>\n  \n  \n  </ion-list>\n\n \n  <br>\n  <br>\n\n  <!-- Botões de voltar e avançar -->\n\n  <div text-center>\n\n    <button ion-button icon-left (click)="GoBack()">\n      <ion-icon name="md-arrow-back"></ion-icon>\n      Voltar\n    </button>\n    \n    <!--\n      <button ion-button icon-center (click)="GotoNextPage()" *ngIf="focusType==\'Culex\'">\n      Próximo &nbsp;   \n      <ion-icon name="md-arrow-forward"></ion-icon>\n    </button>\n    --><!-- &nbsp é para dar espaço --> \n\n    <!--Finalizando formulario se o foco for Aedes -->\n    <button ion-button icon-right (click)="Finish()">\n        Concluir\n        <ion-icon name="exit"></ion-icon>\n      </button>\n\n  </div>\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\tratamento-psa\tratamento-psa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]])
    ], TratamentoPsaPage);
    return TratamentoPsaPage;
}());

//# sourceMappingURL=tratamento-psa.js.map

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActivitiesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_psa_form_psa_form__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_liraa_form_liraa_form__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__start_visit_start_visit__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_api_send_forms_api_send_forms__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_network_network__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ActivitiesPage = /** @class */ (function () {
    // -------------- Construtor -----------------------------
    function ActivitiesPage(navCtrl, navParams, psaProvider, liraaProvider, network, eventCtrl, apiSendFormsProvider, alertCtrl, networkProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.psaProvider = psaProvider;
        this.liraaProvider = liraaProvider;
        this.network = network;
        this.eventCtrl = eventCtrl;
        this.apiSendFormsProvider = apiSendFormsProvider;
        this.alertCtrl = alertCtrl;
        this.networkProvider = networkProvider;
        // -------------- Atributos --------------------------
        this.newFormsToSend = false;
        // Aguarda o evento, fieldDepositsDone, ser publicado/realizado. Para alterar o valor do atributo fieldDepositsDone, alterando o ícone de seta para checkmark.
        eventCtrl.subscribe('haveNewFormsToSend', function (status) {
            // Ativa o botão para enviar dados apenas se estiver conectado à rede.
            if (_this.networkProvider.getNetworkStatus() == "online") {
                _this.newFormsToSend = status;
            }
        });
    }
    // -------------- Métodos -----------------------------
    ActivitiesPage.prototype.ionViewDidLoad = function () {
        // Verificando se há dados no storage; caso sim, o botão de enviar dados é ativado.
        this.psaProvider.getAll();
        this.liraaProvider.getAll();
    };
    ActivitiesPage.prototype.startVisit = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__start_visit_start_visit__["a" /* StartVisitPage */]);
    };
    // Envia dados que estão no banco local para o servidor.
    ActivitiesPage.prototype.sendDataToServer = function () {
        var _this = this;
        console.log("Dados para enviar");
        this.sendForms()
            .then(function (res) {
            _this.presentAlert('Dados Enviados ao Servidor!', '');
            _this.newFormsToSend = false;
        }).catch(function (err) {
            _this.presentAlert('Erro ao Enviar Dados', 'Tente novamente mais tarde');
        });
    };
    // Método responsável por enviar os dados que estão no banco local, dos formulários liraa e psa, para o servidor.
    ActivitiesPage.prototype.sendForms = function () {
        var _this = this;
        return new Promise(function (res, rej) {
            // Envio dos dados para o formulário PSA.
            _this.psaProvider.getAll()
                .then(function (result) {
                _this.listVisit = result;
                // Enviando ao servidor todos os formulários PSA que estão no banco local.
                _this.apiSendFormsProvider.sendAllFormsPsa(_this.listVisit)
                    .then(function (msg) {
                    //this.presentAlert('Dados Enviados ao Servidor!', '');         // Exibe uma mensagem de alerta.
                    console.log("Mensagem Ok PSA:    ");
                    console.log(msg);
                    res(msg);
                }).catch(function (err) {
                    console.log("Erro no envio:    ");
                    console.log(err);
                    rej(err);
                });
            })
                .catch(function () {
                //this.presentAlert('Erro ao Enviar Dados', 'Tente novamente mais tarde');      // Exibe mensagem informando que houve algum problema.
            });
            // Envio dos dados para o formulário LIRAa.
            _this.liraaProvider.getAll()
                .then(function (result) {
                _this.listVisitLiraa = result;
                // Enviando ao servidor todos os formulários PSA que estão no banco local.
                _this.apiSendFormsProvider.sendAllFormsLiraa(_this.listVisitLiraa)
                    .then(function (msg) {
                    console.log("Mensagem Ok LIRAA:    ");
                    console.log(msg);
                    res(msg);
                }).catch(function (err) {
                    console.log("Erro no envio:    ");
                    console.log(err);
                    rej(err);
                });
            })
                .catch(function (err) {
            });
        });
    };
    // Método para exibir alerta na tela.
    ActivitiesPage.prototype.presentAlert = function (titleMsg, subTitleMsg) {
        var alert = this.alertCtrl.create({
            title: titleMsg,
            subTitle: subTitleMsg,
            buttons: ['Ok']
        });
        alert.present();
    };
    ActivitiesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-activities',template:/*ion-inline-start:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\activities\activities.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n\n    <!-- Botão para o side menu -->\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n\n    <ion-title>Atividades</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n    <h3 class="text-custom">Hora de Iniciar as Atividades</h3>\n\n    <img src="../../assets/imgs/navigator.png" class="center">\n\n    <ion-card>\n        <ion-card-content>\n          Nessa seção você deve escolher uma das atividades disponíveis. A opção para envio dos dados estará habilitada logo após a finalização de uma visita\n          e quando ouver conexão com a internet.\n        </ion-card-content>\n      </ion-card>\n  \n\n  <ion-row>\n    <ion-col width-50 style="text-align: right">\n      <button ion-button color="primary" round (click)="startVisit()">\n        Iniciar Visita   \n      </button>\n    </ion-col>\n    <ion-col width-50 style="text-align: left">\n      <button ion-button color="primary" round [disabled]="newFormsToSend == false" (click)="sendDataToServer()">\n        Enviar Dados\n      </button>\n    </ion-col>\n  </ion-row>\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\afs10\Documents\GitLab\geo-saude\front\GeoSaude-App\src\pages\activities\activities.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_psa_form_psa_form__["a" /* PsaFormProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_liraa_form_liraa_form__["a" /* LiraaFormProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */],
            __WEBPACK_IMPORTED_MODULE_6__providers_api_send_forms_api_send_forms__["a" /* ApiSendFormsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_network_network__["a" /* NetworkProvider */]])
    ], ActivitiesPage);
    return ActivitiesPage;
}());

//# sourceMappingURL=activities.js.map

/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return VisitInformationLIRAa; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImmobileLIRAa; });
/* unused harmony export BreendingGroundsAedes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return TpLarvicidesLIRAa; });
// VisitInformationLIRAa contém a estrutura que será utilizada para armazenar informações no storage.
var VisitInformationLIRAa = /** @class */ (function () {
    function VisitInformationLIRAa(places, // Logradouro
        immobile, // Imóvel
        vtAedes, // Vetor aedes. Neste caso (LIRAa), ele só precisará das informações sobre os criadouros.
        depositsRemoved, // Quantidade de depósitos eliminados
        larvicides // Larvicidas 
    ) {
        this.places = places;
        this.immobile = immobile;
        this.vtAedes = vtAedes;
        this.depositsRemoved = depositsRemoved;
        this.larvicides = larvicides; // Larvicidas 
    }
    /**
     * setPlaces
     */
    VisitInformationLIRAa.prototype.setPlace = function (place) {
        this.places = place;
    };
    /**
     * setImmogile
     */
    VisitInformationLIRAa.prototype.setImmobile = function (immobile) {
        this.immobile = immobile;
    };
    /**
     * setVtAedes
     */
    VisitInformationLIRAa.prototype.setVtAedes = function (vtAedes) {
        this.vtAedes = vtAedes;
    };
    /**
     * setDepositsRemoved
     */
    VisitInformationLIRAa.prototype.setDepositsRemoved = function (deposits) {
        this.depositsRemoved = deposits;
    };
    /**
     * setLarvicides
     */
    VisitInformationLIRAa.prototype.setLarvicides = function (tpLarvicides) {
        this.larvicides = tpLarvicides;
    };
    return VisitInformationLIRAa;
}());

// -------------------------------------------------------------------------------------
var ImmobileLIRAa = /** @class */ (function () {
    function ImmobileLIRAa() {
    }
    return ImmobileLIRAa;
}());

/*
export class VtAedes {
  tpBreedingGrounds: BreendingGroundsAedes    // Tipo de criadouros
}
*/
var BreendingGroundsAedes = /** @class */ (function () {
    function BreendingGroundsAedes() {
    }
    return BreendingGroundsAedes;
}());

var TpLarvicidesLIRAa = /** @class */ (function () {
    function TpLarvicidesLIRAa() {
    }
    return TpLarvicidesLIRAa;
}());

//# sourceMappingURL=VisitInformationLiraa.js.map

/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ApiProvider = /** @class */ (function () {
    function ApiProvider(storage, http) {
        this.storage = storage;
        this.http = http;
        //Endereço Base da API (versão Local)
        this.baseLocalApiPath = "http://127.0.0.1:8000/";
        //Endereço Base da API (versão Remota)
        //private baseRemoteApiPath = "";
        //End-Point para Login de Usuário
        this.authEndPoint = "api/login/";
        //End-Point para envio do Formulário LIRAa
        this.liraEndPoint = "api/formularioLIRAa/";
        //End-Point para envio de Formulário PSA
        this.psaEndPoint = "api/formularioPSA/";
        //Chave do Authentication Token no Storage  
        this.authTokenKey = "authToken";
        console.log('Hello ApiProvider Provider');
    }
    //Armazena o token de Autenticação
    ApiProvider.prototype.setAuthenticationToken = function (authToken) {
        return this.storage.set(this.authTokenKey, authToken);
    };
    //Resgata o token de Autenticação
    ApiProvider.prototype.getAuthenticationToken = function () {
        return this.storage.get(this.authTokenKey);
    };
    ApiProvider.prototype.sendAuthenticationData = function (username, senha) {
        //Realizando chamada HTTP direcionada ao Back-End
        return this.http.post(this.baseLocalApiPath + this.authEndPoint, { 'username': username, 'senha': senha });
    };
    //Função de envio do Formulario LIRAa para o BackEnd
    ApiProvider.prototype.sendLIRAaFormToBackEnd = function (visitInformationLIRAa, authToken) {
        //Criando JSON
        var dataJSON = JSON.stringify(visitInformationLIRAa);
        //Adicionando Token ao Header da Requisição
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Authorization': authToken });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: header });
        //Realizando chamada HTTP direcionada ao Back-End
        return this.http.post(this.baseLocalApiPath + this.liraEndPoint, dataJSON, options);
    };
    //Função de envio do Formulario PSA para o BackEnd
    ApiProvider.prototype.sendPSAFormToBackEnd = function (visitInformationPSA, authToken) {
        //Criando JSON
        var dataJSON = JSON.stringify(visitInformationPSA);
        console.log("O JSON que estou enviando:  ");
        console.log(dataJSON);
        //Adicionando Token ao Header da Requisição
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Authorization': authToken });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: header });
        //Realizando chamada HTTP direcionada ao Back-End
        return this.http.post(this.baseLocalApiPath + this.psaEndPoint, dataJSON, options);
    };
    ApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], ApiProvider);
    return ApiProvider;
}());

//# sourceMappingURL=api.js.map

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiSendFormsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__psa_form_psa_form__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__liraa_form_liraa_form__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the ApiSendFormsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ApiSendFormsProvider = /** @class */ (function () {
    //------------------------------------------------------------------
    //     Construtor
    //------------------------------------------------------------------
    function ApiSendFormsProvider(http, psaProvider, liraaProvider) {
        this.http = http;
        this.psaProvider = psaProvider;
        this.liraaProvider = liraaProvider;
        //------------------------------------------------------------------
        //    Atributos
        //------------------------------------------------------------------
        this.urlServer = 'http://localhost:8080/';
        this.endUrlFormPsa = 'psa-form/';
        this.endUrlFormLiraa = 'liraa-form/';
        console.log('Hello ApiSendFormsProvider Provider');
    }
    ApiSendFormsProvider.prototype.sendFormPsa = function (formPsaVisited) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var hds = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            hds.append('Content-Type', 'application/json');
            console.log(formPsaVisited);
            _this.http.post(_this.urlServer + _this.endUrlFormPsa, JSON.stringify(formPsaVisited), { headers: hds })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (error) {
                reject(error);
            });
        });
    };
    ApiSendFormsProvider.prototype.sendAllFormsPsa = function (formsPsaVisited) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var hds = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            hds.append('Content-Type', 'application/json');
            console.log(formsPsaVisited);
            formsPsaVisited.forEach(function (value) {
                var visitPSAInfo = value.visitInformationPSA;
                _this.http.post(_this.urlServer + _this.endUrlFormPsa, JSON.stringify(visitPSAInfo), { headers: hds })
                    .map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    resolve(data);
                    // Deleta todos os registros locais.
                    _this.psaProvider.remove(value.key);
                }, function (error) {
                    reject(error);
                });
            });
        });
    };
    ApiSendFormsProvider.prototype.sendAllFormsLiraa = function (formsLiraaVisited) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var hds = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            hds.append('Content-Type', 'application/json');
            console.log(formsLiraaVisited);
            formsLiraaVisited.forEach(function (value) {
                var visitLiraaInfo = value.visitInformationLIRAa;
                _this.http.post(_this.urlServer + _this.endUrlFormLiraa, JSON.stringify(visitLiraaInfo), { headers: hds })
                    .map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    resolve(data);
                    // Deleta todos os registros locais.
                    _this.liraaProvider.remove(value.key);
                }, function (error) {
                    reject(error);
                });
            });
        });
    };
    ApiSendFormsProvider.prototype.sendFormLiraa = function (formLiraaVisited) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var hds = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            hds.append('Content-Type', 'application/json');
            _this.http.post(_this.urlServer + _this.endUrlFormLiraa, JSON.stringify(formLiraaVisited), { headers: hds })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (error) {
                reject(error);
            });
        });
    };
    ApiSendFormsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3__psa_form_psa_form__["a" /* PsaFormProvider */],
            __WEBPACK_IMPORTED_MODULE_4__liraa_form_liraa_form__["a" /* LiraaFormProvider */]])
    ], ApiSendFormsProvider);
    return ApiSendFormsProvider;
}());

//# sourceMappingURL=api-send-forms.js.map

/***/ })

},[353]);
//# sourceMappingURL=main.js.map