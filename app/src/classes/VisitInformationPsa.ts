// VisitInformationPSA contém a estrutura que será utilizada para armazenar informações no storage.


export class VisitInformationPSA {
  visitInformationPSA: any;
  key: any;

  constructor(
    private places: string,              // Logradouro
    private immobile: Immobile,          // Imóvel
    private trash: Trash,                // Lixo
    private vtAedes: VtAedes,            // Vetor aedes
    private vtCulex: VtCulex,            // Vetor cúlex
    private ovitraps: number,            // Ovitrampas
    private larvicides: TpLarvicides,    // Larvicidas 
    ) {}
  
    

    /**
     * setPlace
     */
    public setPlace(place: string) {
      this.places = place;
    }

    /**
     * setImmobile
     */
    public setImmobile(immobile: Immobile) {
     this.immobile = immobile; 
    }

    /**
     * setTrash
     */
    public setTrash(trash: Trash) {
      this.trash = trash;
    }
  
    /**
     * setVtAedes
     */
    public setVtAedes(vtAedes: VtAedes) {
      this.vtAedes = vtAedes;
    }

    /**
     * setVtCulex
     */
    public setVtCulex(vtCulex: VtCulex) {
      this.vtCulex = vtCulex;
    }

    /**
     * setOvitraps
     */
    public setOvitraps(ovitraps: number) {
      this.ovitraps = ovitraps;
    }

    /**
     * setLarvicides
     */
    public setLarvicides(larvicides: TpLarvicides) {
      this.larvicides = larvicides;
    }
  
  }
  
  
  // -------------------------------------------------------------------------------------
  
  export class Immobile {
    tpImmobile: number;
    situationImmobile: number;
    dateVisit: string;
  }
  
  export class Trash {
    tpTrash: number;
    packaging: number;                  // Acondicionamento
    domicileTrashDestiny: number;       // Se o lixo for domiciliar, qual o destino?        
  }
  
  export class VtAedes {
    tpBreedingGrounds: BreendingGroundsAedes    // Tipo de criadouros
    tpTreatment: Treatment;
  }
  
  export class BreendingGroundsAedes {
    A1: number;
    A2: number;
    B: number;
    C: number;
    D1: number;
    D2: number;
    E: number;
  }
  
  export class Treatment {
    positive: number;
    mechanical: number;
    biological: number;
    chemical: number;
  }
  
  
  export class VtCulex {
    tpBreedingGrounds: BreendingGroundsCulex;
    tpTreatment: Treatment;
  }
  
  export class BreendingGroundsCulex {
    fosse: boolean = false;                             // Fossa
    cistern: boolean = false ;                          // Cisterna
    channelDichGutters: boolean = false;                // Canal-Canaletas-Valas
    puddle: boolean = false;                            // Charco, poça de água
    inspectionBoxDrainage: boolean = false;             // Caixa de inspeção - Drenagem
  }
  
  export class TpLarvicides {
    BTiG_gramas: number;
    BTiG_depositos: number;
    
    BTiWDg_gramas: number;
    BTiWDg_depositos: number;
  
    BsG_gramas: number;
    BsG_depositos: number;
  }