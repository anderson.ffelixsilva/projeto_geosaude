/*
    Modelo de dados para o Agentes de Saúde
*/

import { ScheduledLocation } from "./scheduled-location-model";

export class AgentModel {

    constructor(
        public id: number,
        public firstName: string, 
        public lastName: string, 
        public email: string,
        public district: string,
        public schedule: ScheduledLocation[]){
    }

    getFullName(){
        return this.firstName + " " + this.lastName;
    }

    getDistrict() {
        return this.district;
    }

}
