import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { TratamentoPsaPage } from '../tratamento-psa/tratamento-psa';
import { BreendingGroundsCulex, VisitInformationPSA } from '../../classes/VisitInformationPsa';


@IonicPage()
@Component({
  selector: 'page-culex',
  templateUrl: 'culex.html',
})
export class CulexPage {

  // ------------ Atributos ----------------
  private breendingGroundCulex: BreendingGroundsCulex;
  private visitInformation: VisitInformationPSA;
  

  // ------------ Métodos ----------------
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {

    this.breendingGroundCulex = new BreendingGroundsCulex();
    this.visitInformation = this.navParams.get('visitInformation');
    console.log(this.visitInformation);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CulexPage');
  }


  // Método para voltar a tela anterior.

  GoBack(){
    this.navCtrl.pop();
  }


  // Método para ir a próxima tela.

  GotoNextPage(){
    
    console.log(this.visitInformation);

    this.navCtrl.push(TratamentoPsaPage,{ foco:'Culex', visitInformation: this.visitInformation, breendingGroundCulex: this.breendingGroundCulex });
  }

}
