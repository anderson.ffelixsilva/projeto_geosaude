import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { ActivitiesPage } from '../activities/activities';
import { ImmobilePage } from '../immobile/immobile';
import { AedesPage } from '../aedes/aedes';
import { CulexPage } from '../culex/culex';
import { TrashPage } from '../trash/trash';
import { PsaFormProvider } from '../../providers/psa-form/psa-form';
import { LarvicidesPage } from '../larvicides/larvicides';
import { LiraaFormProvider } from '../../providers/liraa-form/liraa-form';
import { VisitInformationLIRAa } from '../../classes/VisitInformationLiraa';
import { Events } from 'ionic-angular';
import { VisitInformationPSA } from '../../classes/VisitInformationPsa';
import { ApiProvider } from '../../providers/api/api';
import { ApiSendFormsProvider } from '../../providers/api-send-forms/api-send-forms';
import { NetworkProvider } from '../../providers/network/network';
import { MyApp } from '../../app/app.component';

@IonicPage()
@Component({
  selector: 'page-start-forms',
  templateUrl: 'start-forms.html',
})
export class StartFormsPage {

  // -------------- Atributos --------------------------
  private formType: string;
  private existsPage: string;
  private address: string;
  private visitInformation;
  public fieldImmobileDone: boolean = false;
  private fieldTrashDone: boolean = false;
  private fieldAedesDone: boolean = false;
  private fieldCulexDone: boolean = false;
  private fieldOvitrapsDone: boolean = false;
  private fieldLarvicidesDone: boolean = false;
  private fieldDepositsDone: boolean = false;


  // -------------- Métodos -----------------------------

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private psaFormProvider: PsaFormProvider,
    private liraaFormProvider: LiraaFormProvider,
    private apiProvider: ApiProvider,
    private apiSendFormsProvider: ApiSendFormsProvider,
    public networkProvider: NetworkProvider,
    public toastCtrl: ToastController) {


    // Os eventos abaixo, são responsáveis por realizar a troca do ícone que fica ao lado de cada campo, de forma a confirmar o preenchimendo do campo.


    // Aguarda o evento, fieldImmobileDone, ser publicado. Para alterar o valor do atributo fieldImmobileDone, alterando o ícone de seta para checkmark.
    events.subscribe('fieldImmobileDone', (visitInformationImmobile) => {
      this.fieldImmobileDone = true;
      this.visitInformation = visitInformationImmobile;

    });

    // Aguardo o evento, fieldTrashDone, ser publicado/realizado. Para alterar o valor do atributo fieldTrashDone, alterando o ícone de seta para checkmark.
    events.subscribe('fieldTrashDone', (visitInformationTrash) => {
      this.fieldTrashDone = true;
      this.visitInformation = visitInformationTrash;

    });

    // Aguarda o evento, fieldAedesDone, ser publicado/realizado. Para alterar o valor do atributo fieldAedesDone, alterando o ícone de seta para checkmark.
    events.subscribe('fieldAedesDone', (visitInformationAedes) => {
      this.fieldAedesDone = true;
      this.visitInformation = visitInformationAedes;

    });

    // Aguarda o evento, fieldCulexDone, ser publicado/realizado. Para alterar o valor do atributo fieldCulexDone, alterando o ícone de seta para checkmark
    events.subscribe('fieldCulexDone', (visitInformationCulex) => {
      this.fieldCulexDone = true;
      this.visitInformation = visitInformationCulex;

    });

    // Aguarda o evento, fieldOvitrapsDone, ser publicado/realizado. Para alterar o valor do atributo fieldOvitraps, alterando o ícone de seta para checkmark.
    events.subscribe('fieldOvitrapsDone', (visitInformationOvitraps) => {
      this.fieldOvitrapsDone = true;
      this.visitInformation = visitInformationOvitraps;

    });

    // Aguarda o evento, fieldLarvicidesDone, ser publicado/realizado. Para alterar o valor do atributo fieldLarvicidesDone, alterando o ícone de seta para checkmark.
    events.subscribe('fieldLarvicidesDone', (visitInformationLarvicides) => {
      this.fieldLarvicidesDone = true;
      this.visitInformation = visitInformationLarvicides;

    });

    // Aguarda o evento, fieldDepositsDone, ser publicado/realizado. Para alterar o valor do atributo fieldDepositsDone, alterando o ícone de seta para checkmark.
    events.subscribe('fieldDepositsDone', (visitInformationDeposits) => {
      this.fieldDepositsDone = true;
      this.visitInformation = visitInformationDeposits;

    });
  }



  // ionViewDidLoad executa quando a página é carregada. Esse evento acontece apenas uma vez quando a página é criada.


  ionViewDidLoad() {

    //Recebe informação sobre o tipo de formulario selecionado: LIRAa ou PSA. E sobre o endereço.
    this.formType = this.navParams.get("formName");
    this.address = this.navParams.get("address");

    // Caso o preenchimento do formulário já tenha sido iniciado, oculta o botão de voltar para a página de SelectForm

    /*if (this.navParams.get("pageFlag")) {
      this.visitInformation = this.navParams.get("visitInformation");
      this.existsPage = this.navParams.get("pageFlag");
    } else {
      this.existsPage = "true";
    }*/


    // Verificando o tipo do formulário para criar a instância de acordo com o formulário escolhido.

    if (this.formType == 'PSA') {
      this.visitInformation = new VisitInformationPSA(null, null, null, null, null, null, null);      // Criando a instância para o formulário PSA.
      this.visitInformation.setPlace(this.address);          // Adicionando o endereço ao campo places.


    } else if (this.formType == 'LIRAa') {
      console.log("Criando formulário liraa"); ""
      this.visitInformation = new VisitInformationLIRAa(null, null, null, null, null);    // Criando a instância para o formulário LIRAa.
      //this.visitInformation.places = this.address;            // Adicionando o endereço ao campo places.
      this.visitInformation.setPlace(this.address);

    }


  };



  // Método que chama a tela do Imóvel (ImmobilePage).

  GotoImovelPage() {
    console.log(this.visitInformation);
    this.navCtrl.push(ImmobilePage, { formType: this.formType, visitInformation: this.visitInformation, context: this });
  }


  // Método que chama a tela referentes aos lixos encontrados (LixoPage).

  GotoLixoPage() {
    console.log("passagem imovel: " + this.formType);
    this.navCtrl.push(TrashPage, { formType: this.formType, visitInformation: this.visitInformation });
  }

  // Método que chama a tela do Aedes (AedesPage).

  GotoAedesPage() {
    console.log("passagem imovel: " + this.formType);
    console.log(this.visitInformation);
    this.navCtrl.push(AedesPage, { formType: this.formType, visitInformation: this.visitInformation });
  }


  // Método que chama a tela do Cúlex (CulexPage).

  GotoCulexPage() {
    this.navCtrl.push(CulexPage, { visitInformation: this.visitInformation });
  }



  // Método que abre a tela referente aos larvicidas.


  GotoLarvicidasPage() {
    this.navCtrl.push(LarvicidesPage, { formType: this.formType, visitInformation: this.visitInformation });
  }



  // Número de ovitrampas - Para o formulário PSA.


  GotoOvitrampasEvent() {

    // Coletando o número de ovitrampas por meio de um Alert.

    let alert = this.alertCtrl.create({
      title: 'Digite o Número de Ovitrampas: ',
      inputs: [                                     // Parâmetros que guarda a entrada.
        {
          name: 'ovitrampas',                       // Número de ovitrampas fica armazenado aqui.
          placeholder: 'Ovitrampas',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log("Clicou em cancelar");
          }
        },
        {                           // Após o clique no botão Ok é atribuindo o valor de ovitrampas no objeto visitInformatio.
          text: 'Ok',
          role: 'ok',
          handler: data => {
            this.visitInformation.setOvitraps(data.ovitrampas);
            this.fieldOvitrapsDone = true;
          }
        }
      ]
    });
    alert.present();

  }



  // Número de depósitos eliminados - Formulário LIRAa.


  GotoDepositosEvent() {

    let alert = this.alertCtrl.create({
      title: 'Número de Depósitos Eliminados: ',            // Título do alert.
      inputs: [                                             // Tag do alert.
        {
          name: 'depositos',
          placeholder: 'Depósitos Eliminados'
        }
      ],
      buttons: [                                          // Botões do alert.
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log("Clicou em cancelar");
          }
        },
        {
          text: 'Ok',
          role: 'ok',
          handler: data => {
            this.visitInformation.setDepositsRemoved(data.depositos);          // Obtém o que foi digitado e armazena no objeto VisitInformationLIRAa.
            this.fieldDepositsDone = true;
          }
        }
      ]
    });
    alert.present();

  }



  // Volta para a tela de seleção do formulário (SelectFormPage).


  GotoBackSelectFormPage() {

    let alert = this.alertCtrl.create({
      title: 'Cancelar Preenchimento do Formulário',
      message: 'Você tem certeza que deseja cancelar o preenchimento do formulário?',
      buttons: [
        {
          text: 'Não',
          role: 'dontCancel',
          handler: () => {
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

  // Método que finalizar a visita
  GotoFinalizeVisit() {
    // Confirma se o usuário deseja encerrar o formulário.
    let alert = this.alertCtrl.create({
      title: 'Concluir o Preenchimento do Formulário',
      message: 'Deseja concluir o preenchimento do formulário?',
      buttons: [
        {
          text: 'Não',
          role: 'dontCancel',
          handler: () => {
          }
        },
        {
          text: 'Sim',
          handler: () => {


            // Salvando as informações no armazenamento (storage).

            if (this.formType == 'PSA') {
              // Se tiver conexão com a internet, envia dados ao servidor.
              if (this.networkProvider.getNetworkStatus() == "online") {      // Obtém status da rede.      
                //Recupera token de Seguranca
                this.apiProvider.getAuthenticationToken().then( token => {
                    this.apiProvider.sendPSAFormToBackEnd(this.visitInformation, token).subscribe(
                      data => {
                        this.presentAlert('Visita Finalizada com Sucesso', 'Dados enviados ao servidor!');
                        console.log("PSA: enviou os dados ao server");
                      },
                      error => {
                        this.psaFormProvider.insert(this.visitInformation).then(() => {
                          this.events.publish('haveNewFormsToSend', true);
  
                          console.log("PSA: não conseguiu enviar os dados ao server, servidor off");
  
                          this.presentAlert('Erro ao enviar Formulario', 'Tente novamente mais tarde!');
                        });
                      }
                    );
                });
                



               /* this.apiSendFormsProvider.sendFormPsa(this.visitInformation)  // Enviando ao Servidor.
                  .then((res) => {
                    this.presentAlert('Visita Finalizada com Sucesso', 'Dados enviados ao servidor!');

                    console.log("PSA: enviou os dados ao server");
                  })
                  .catch((err) => {                                           // Se houver algum problema durante a transfência ao servidor, armazena os dados localmente.
                    this.psaFormProvider.insert(this.visitInformation).then(() => {
                      this.events.publish('haveNewFormsToSend', true);

                      console.log("PSA: não conseguiu enviar os dados ao server");

                      this.presentAlert('Visita Finalizada com Sucesso', '');
                    });       // Inserindo no Storage.
              

                  });
               */

                // Caso não tenha conexão com a internet, armazena localmente.  
              } else {
                this.psaFormProvider.insert(this.visitInformation).then(() => {
                  this.events.publish('haveNewFormsToSend', true);

                  console.log("PSA: não conseguiu enviar os dados ao server, servidor off");

                  this.presentAlert('Visita Finalizada com Sucesso', '');
                });       // Inserindo no Storage.


              }

            }
            else {
              // Se tiver conexão com a internet, envia dados ao servidor.
              if (this.networkProvider.getNetworkStatus() == "online") {
                //Recupera token de Segurança
                this.apiProvider.getAuthenticationToken().then((token) => {
                  //Envia formulário ao BackEnd
                  this.apiProvider.sendLIRAaFormToBackEnd(this.visitInformation, token).subscribe(
                    //Sucesso
                    data => {
                    this.presentAlert('Visita Finalizada com Sucesso', 'Dados enviados ao servidor!');
                    console.log("LIRAA: enviou os dados ao server");
                    },

                    //Erro ao enviar formulario, salva no Storage para enviar mais tarde
                    error => {
                      this.liraaFormProvider.insert(this.visitInformation).then(() => {
                        this.events.publish('haveNewFormsToSend', true);

                        console.log("LIRAA: não conseguiu enviar os dados ao server, servidor off");

                        this.presentAlert('Erro ao enviar Formulario', 'Tente novamente mais tarde!');
                      });
                    }
                  );

                });
                
                // Caso não tenha conexão com a internet, armazena localmente.
              } else {
                // Inserindo no Storage.
                this.liraaFormProvider.insert(this.visitInformation).then(() => {
                  this.events.publish('haveNewFormsToSend', true);

                  console.log("LIRAA: não conseguiu enviar os dados ao server, servidor off");

                  this.presentAlert('Visita Finalizada com Sucesso', '');
                });
              }
            }

          }
        }
      ]
    });
    alert.present();

  }

  public presentAlert(titleMsg: string, subTitleMsg: string) {
    let alert = this.alertCtrl.create({
      title: titleMsg,
      subTitle: subTitleMsg,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.events.publish('newForms:true');
            console.log("encerrando a visita");

            //Retira as páginas anteriores da pilha e volta para a TabsPage.
            this.navCtrl.setRoot(ActivitiesPage);
            this.navCtrl.popToRoot();
          }
        }]
    });
    alert.present();
  }

}
