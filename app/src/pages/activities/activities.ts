import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { PsaFormProvider, VisitInformationPSAList } from '../../providers/psa-form/psa-form';
import { LiraaFormProvider, VisitInformationLIRAaList } from '../../providers/liraa-form/liraa-form';
import { Network } from '@ionic-native/network';
import { StartVisitPage } from '../start-visit/start-visit';
import { ApiSendFormsProvider } from '../../providers/api-send-forms/api-send-forms';
import { NetworkProvider } from '../../providers/network/network';
import { ApiProvider } from '../../providers/api/api';


@IonicPage()
@Component({
  selector: 'page-activities',
  templateUrl: 'activities.html',
})
export class ActivitiesPage {


  // -------------- Atributos --------------------------



  private newFormsToSend: boolean = false;

  // Testando o provider
  private listVisit: any;
  private listVisitLiraa: VisitInformationLIRAaList[];



  // -------------- Construtor -----------------------------



  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private psaProvider: PsaFormProvider,
    private liraaProvider: LiraaFormProvider,
    public network: Network,
    public eventCtrl: Events,
    public apiSendFormsProvider: ApiSendFormsProvider,
    private apiProvider: ApiProvider,
    public alertCtrl: AlertController,
    private networkProvider: NetworkProvider) {


    // Aguarda o evento, fieldDepositsDone, ser publicado/realizado. Para alterar o valor do atributo fieldDepositsDone, alterando o ícone de seta para checkmark.
    eventCtrl.subscribe('haveNewFormsToSend', (status) => {
      // Ativa o botão para enviar dados apenas se estiver conectado à rede.
      if (this.networkProvider.getNetworkStatus() == "online") {
        this.newFormsToSend = status;
      }
    });
  }



  // -------------- Métodos -----------------------------



  ionViewDidLoad() {
    // Verificando se há dados no storage; caso sim, o botão de enviar dados é ativado.
    this.psaProvider.getAll()
    this.liraaProvider.getAll();
  }


  startVisit() {
    this.navCtrl.push(StartVisitPage);
  }


  // Envia dados que estão no banco local para o servidor.

  sendDataToServer() {
    console.log("Dados para enviar");

    this.sendForms()
      .then((res) => {
        this.presentAlert('Dados Enviados ao Servidor!', '');
        this.newFormsToSend = false;
      }).catch((err) => {
        this.presentAlert('Erro ao Enviar Dados', 'Tente novamente mais tarde');
      })
  }



  // Método responsável por enviar os dados que estão no banco local, dos formulários liraa e psa, para o servidor.

  sendForms() {
    return new Promise((res, rej) => {
      // Envio dos dados para o formulário PSA.
      this.psaProvider.getAll()
        .then((result) => {
          this.listVisit = result;
          // Enviando ao servidor todos os formulários PSA que estão no banco local.
          this.apiProvider.getAuthenticationToken().then( token => {
            this.listVisit.forEach(visit => {
              
              this.apiProvider.sendPSAFormToBackEnd(visit.getVisitInformation(), token).subscribe(
                data => {
                  this.presentAlert('Sucesso', 'Dados enviados ao servidor!');
                  console.log("PSA: enviou os dados ao server");

                  let key = visit.getKey();
                  this.psaProvider.remove(key);
                },
                error => {
                    console.log("PSA: não conseguiu enviar os dados ao server, servidor off");
                    this.presentAlert('Erro ao enviar Formularios', 'Tente novamente mais tarde!');
                }
              ); 

            });            
        });
      });
        //   this.apiSendFormsProvider.sendAllFormsPsa(this.listVisit)
        //     .then((msg) => {                                                   // Se ocorreu tudo bem com o envio.
        //       //this.presentAlert('Dados Enviados ao Servidor!', '');         // Exibe uma mensagem de alerta.
        //       console.log("Mensagem Ok PSA:    ");
        //       console.log(msg);
        //       res(msg);
        //     }).catch((err) => {
        //       console.log("Erro no envio:    ");
        //       console.log(err);
        //       rej(err);
        //     });

        // })
        // .catch(() => {                                                                  // Se ocorreu algum problema durante o envio.
        //   this.presentAlert('Erro ao Enviar Dados', 'Tente novamente mais tarde');      // Exibe mensagem informando que houve algum problema.
        // });

      // Envio dos dados para o formulário LIRAa.
      this.liraaProvider.getAll()
        .then((result) => {
          this.listVisitLiraa = result;

          this.apiProvider.getAuthenticationToken().then( token => {
            this.listVisitLiraa.forEach(visit => {
              
              this.apiProvider.sendLIRAaFormToBackEnd(visit.getVisitInformation(), token).subscribe(
                data => {
                  this.presentAlert('Sucesso', 'Dados enviados ao servidor!');
                  console.log("LIRAa: enviou os dados ao server");
                  let key = visit.getKey();
                  this.liraaProvider.remove(key);
                },
                error => {
                    console.log("LIRAa: não conseguiu enviar os dados ao server, servidor off");
                    this.presentAlert('Erro ao enviar Formularios', 'Tente novamente mais tarde!');
                }); 

            });            
        });

          // Enviando ao servidor todos os formulários PSA que estão no banco local.
        //   this.apiSendFormsProvider.sendAllFormsLiraa(this.listVisitLiraa)
        //     .then((msg) => {                                                   // Se ocorreu tudo bem com o envio.
        //       console.log("Mensagem Ok LIRAA:    ");
        //       console.log(msg);
        //       res(msg);
        //     }).catch((err) => {
        //       console.log("Erro no envio:    ");
        //       console.log(err);
        //       rej(err);
        //     });
        // })
        // .catch((err) => {                                                                  // Se ocorreu algum problema durante o envio.

      });


    });

  
}


  // Método para exibir alerta na tela.
  public presentAlert(titleMsg: string, subTitleMsg: string) {
    let alert = this.alertCtrl.create({
      title: titleMsg,
      subTitle: subTitleMsg,
      buttons: ['Ok']
    });
    alert.present();
  }

}
