import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { VisitInformationLIRAa } from '../../classes/VisitInformationLiraa';
import { VisitInformationPSA } from '../../classes/VisitInformationPsa';



@Injectable()
export class ApiProvider {


  //Endereço Base da API (versão Local)
  private baseLocalApiPath = "http://127.0.0.1:8000/";

  //Endereço Base da API (versão Remota)
  //private baseRemoteApiPath = "";
  
  //End-Point para Login de Usuário
  private authEndPoint = "api/login/";

  //End-Point para envio do Formulário LIRAa
  private liraEndPoint = "api/formularioLIRAa/"

  //End-Point para envio de Formulário PSA
  private psaEndPoint = "api/formularioPSA/"

  //Chave do Authentication Token no Storage  
  private authTokenKey = "authToken";


  constructor(public storage: Storage, public http: Http) {
    console.log('Hello ApiProvider Provider');
  }

  //Armazena o token de Autenticação
  public setAuthenticationToken(authToken: String) {
    return this.storage.set(this.authTokenKey, authToken);
  }

  //Resgata o token de Autenticação
  public getAuthenticationToken() {
    return this.storage.get(this.authTokenKey);
  }

  public sendAuthenticationData(username: string, senha: string) {
    //Realizando chamada HTTP direcionada ao Back-End
    return this.http.post(this.baseLocalApiPath + this.authEndPoint, { 'username': username, 'senha': senha });
  }

  //Função de envio do Formulario LIRAa para o BackEnd
  public sendLIRAaFormToBackEnd(visitInformationLIRAa: VisitInformationLIRAa, authToken: String) {
    //Criando JSON
    let dataJSON = JSON.stringify(visitInformationLIRAa);

    //Adicionando Token ao Header da Requisição
    let header = new Headers({ 'Authorization': authToken });
    let options = new RequestOptions({ headers: header });

    //Realizando chamada HTTP direcionada ao Back-End
    return this.http.post(this.baseLocalApiPath + this.liraEndPoint, dataJSON, options);
  }

  //Função de envio do Formulario PSA para o BackEnd
  public sendPSAFormToBackEnd(visitInformationPSA: VisitInformationPSA, authToken: String) {
    //Criando JSON
    let dataJSON = JSON.stringify(visitInformationPSA);

    //Adicionando Token ao Header da Requisição
    let header = new Headers({ 'Authorization': authToken });
    let options = new RequestOptions({ headers: header });

    //Realizando chamada HTTP direcionada ao Back-End
    return this.http.post(this.baseLocalApiPath + this.psaEndPoint, dataJSON, options);
  }

}