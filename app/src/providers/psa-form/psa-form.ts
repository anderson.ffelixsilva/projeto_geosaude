import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { VisitInformationPSA } from '../../classes/VisitInformationPsa';
import {  Events } from 'ionic-angular';


@Injectable()
export class PsaFormProvider {

// ----------------- Atributos ------------------------------------------------------------
  private psaForms: Array<Object>;
  private psaFormDb: any;


// ----------------- Construtor ------------------------------------------------------------

  constructor(
    private datepipe: DatePipe,
    private eventsCtrl: Events) { 
    
    // Criando novo storage.
    this.psaFormDb = new Storage({
      name: '__psadb',
      storeName: '__psaForm',
      driverOrder: ['indexeddb', 'sqlite', 'websql ']
    });
  }

// ----------------- Métodos ------------------------------------------------------------

  public insert(visitInformationPSA: VisitInformationPSA) {
    // Gerando a chave de armazenamento.
    let key = this.datepipe.transform(new Date(), "ddMMyyyyHHmmss");

    return this.save(key, visitInformationPSA);
  }

  public update(key: string, visitInformationPsa: VisitInformationPSA) {
    return this.save(key, visitInformationPsa);
  }

  private save(key: string, visitInformationPSA: VisitInformationPSA) {
    return this.psaFormDb.set(key, visitInformationPSA);
  }

  public remove(key: string) {
    return this.psaFormDb.remove(key);

  }

  public getAll() {
    let visitsInformations: VisitInformationPSAList[] = [];                  // Declarando a lista que guardará as informações que serão retornadas

    // Lendo cada linha da estrutura do storage.
    return this.psaFormDb.forEach((value: VisitInformationPSA, key: string, iterationNumber: Number) => {
     // Publica um evento para avisar que há dados no storage que precisam ser enviados ao servidor.
      this.eventsCtrl.publish('haveNewFormsToSend', true);
      // Declarando objeto que receberá o valor dos parâmetros de cada linha.
      let visit = new VisitInformationPSAList(null, null);
      visit.setKey(key);                                // Recebe a chave.
      visit.setVisitInformation(value);                 // Recebe o valor, neste caso é o objeto VisitInformationPSA.

      visitsInformations.push(visit);                   // Coloca objeto com as informações das visitas na lista.
    })
      .then(() => {
        return Promise.resolve(visitsInformations);
      })
      .catch((error) => {
        console.log(error);
        return Promise.reject(error);
      });
  }




  /*
  
    public insert(visitInformationPSA: VisitInformationPSA) {
  
      // Gerando a chave de armazenamento.
      let key =  this.datepipe.transform(new Date(), "ddMMyyyyHHmmss");
  
      return this.save(key, visitInformationPSA);
  
    }
  
    public update(key: string, visitInformationPsa: VisitInformationPSA) {
      return this.save(key, visitInformationPsa);
  
    }
  
    private save(key: string, visitInformationPSA: VisitInformationPSA) {
      return this.storage.set(key, visitInformationPSA);
    }
  
    public remove(key: string){
      return this.storage.remove(key);
  
   }
  
   public getAll() {
  
    let visitsInformations: VisitInformationPSAList [] = [];                  // Declarando a lista que guardará as informações que serão retornadas
  
    // Lendo cada linha da estrutura do storage.
    return this.storage.forEach( (value: VisitInformationPSA, key: string, iterationNumber: Number) => {
      
      // Declarando objeto que receberá o valor dos parâmetros de cada linha.
      let visit = new VisitInformationPSAList(null, null);
      visit.setKey(key);                                    // Recebe a chave.
      visit.setVisitInformation(value);                  // Recebe o valor, neste caso é o objeto VisitInformationPSA.
  
      visitsInformations.push(visit);                   // Coloca objeto com as informações das visitas na lista.
    } )
      .then(() => {
        return Promise.resolve(visitsInformations);
      })
      .catch( (error) => {
        console.log(error);
        return Promise.reject(error);
      });
   }
  
   */

}



// Está classe será utilizada quando for necessário recuperar o que está armazenado no storage.


export class VisitInformationPSAList {

  constructor(
    private key: string,
    private visitInformationPSA: VisitInformationPSA
  ) { }


  //Getters
  public getKey() {
    return this.key;
  }

  public getVisitInformation() {
    return this.visitInformationPSA;
  }


  /**
   * setKey
   */
  public setKey(key: string) {
    this.key = key;
  }

  /**
   * setVisitInformarionPSA
   */
  public setVisitInformation(visit: VisitInformationPSA) {
    this.visitInformationPSA = visit;
  }


}


