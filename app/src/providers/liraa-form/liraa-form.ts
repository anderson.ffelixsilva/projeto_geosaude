import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { VisitInformationLIRAa } from '../../classes/VisitInformationLiraa';
import { Events } from 'ionic-angular';



@Injectable()
export class LiraaFormProvider {


  // -------------- Atributos --------------------------

  private liraaFormDb: any;
  

  // -------------- Construtor -----------------------------


  constructor(
    private datepipe: DatePipe,
    private eventsCtrl: Events) {

    // Criando novo banco local.
    this.liraaFormDb = new Storage({
      name: '__liraadb',
      storeName: '__liraaForm',
      driverOrder: ['indexeddb', 'sqlite', 'websql ']
    });

  }
  

  // -------------- Métodos --------------------------

  // Insere novo dados no banco local.
  public insert(visitInformationLIRAa: VisitInformationLIRAa) {
    // Gerando a chave de armazenamento.
    let key = this.datepipe.transform(new Date(), "ddMMyyyyHHmmss");
    return this.save(key, visitInformationLIRAa);

  }

  // Atualiza dados do banco local.
  public update(key: string, visitInformationLIRAa: VisitInformationLIRAa) {
    return this.save(key, visitInformationLIRAa);

  }

  // Salva dados no banco local.
  private save(key: string, visitInformationLIRAa: VisitInformationLIRAa) {
    return this.liraaFormDb.set(key, visitInformationLIRAa);
  }

  // Deleta dados do banco local.
  public remove(key: string) {
    return this.liraaFormDb.remove(key);

  }

  // Busca todos os dados que estão no banco local.
  public getAll() {
    // Declarando a lista que guardará as informações que serão retornadas
    let visitsInformations: VisitInformationLIRAaList[] = [];                  

    // Lendo cada linha da estrutura do storage.
    return this.liraaFormDb.forEach((value: VisitInformationLIRAa, key: string, iterationNumber: Number) => {
      // Publica um evento caso avisando que há dados para ser enviados ao servidor.
      this.eventsCtrl.publish('haveNewFormsToSend', true);

      // Declarando objeto que receberá o valor dos parâmetros de cada linha.
      let visit = new VisitInformationLIRAaList(null, null);
      visit.setKey(key);                           // Recebe a chave.
      visit.setVisitInformation(value);            // Recebe o valor, neste caso é o objeto VisitInformationLIRAa.

      visitsInformations.push(visit);               // Coloca objeto com as informações das visitas na lista.
    })
      .then((msg) => {
        // Retorna lista com todos os formulários liraa do banco local.
        return Promise.resolve(visitsInformations);
      })
      .catch((error) => {
        // Retorna o erro, caso ocorra.
        return Promise.reject(error);
      });
  }



  /*
    public insert(visitInformationLIRAa: VisitInformationLIRAa) {
  
      // Gerando a chave de armazenamento.
      let key = this.datepipe.transform(new Date(), "ddMMyyyyHHmmss");
  
      return this.save(key, visitInformationLIRAa);
  
    }
  
  
  
  
    public update(key: string, visitInformationLIRAa: VisitInformationLIRAa) {
      return this.save(key, visitInformationLIRAa);
  
    }
  
  
  
  
    private save(key: string, visitInformationLIRAa: VisitInformationLIRAa) {
      return this.storage.set(key, visitInformationLIRAa);
    }
  
  
  
  
    public remove(key: string) {
      return this.storage.remove(key);
  
    }
  
  
  
  
  
    public getAll() {
  
      let visitsInformations: VisitInformationLIRAaList[] = [];                  // Declarando a lista que guardará as informações que serão retornadas
  
      // Lendo cada linha da estrutura do storage.
      return this.storage.forEach((value: VisitInformationLIRAa, key: string, iterationNumber: Number) => {
  
        // Declarando objeto que receberá o valor dos parâmetros de cada linha.
        let visit = new VisitInformationLIRAaList(null, null);
        visit.setKey(key);                                       // Recebe a chave.
        visit.setVisitInformation(value);                   // Recebe o valor, neste caso é o objeto VisitInformationLIRAa.
  
        visitsInformations.push(visit);                       // Coloca objeto com as informações das visitas na lista.
      })
        .then(() => {
          return Promise.resolve(visitsInformations);
        })
        .catch((error) => {
          return Promise.reject(error);
        });
    }
  */

}

// Está classe será utilizada quando for necessário recuperar o que está armazenado no storage.
export class VisitInformationLIRAaList {

  constructor(
    private key: string,
    private visitInformationLIRAa: VisitInformationLIRAa) {
  }

  //Getters
  public getKey() {
    return this.key;
  }

  public getVisitInformation() {
    return this.visitInformationLIRAa;
  }

  /**
   * setKey
   */
  public setKey(key: string) {
    this.key = key;
  }

  /**
   * setVisitInformationLIRAa
   */
  public setVisitInformation(visit: VisitInformationLIRAa) {
    this.visitInformationLIRAa = visit;
  }
}

