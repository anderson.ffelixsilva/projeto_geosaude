# GAppServer
The Web Server uses Django and MongoDB
Make sure you have MongoDB 3.4 or higher 

To recreate the environment:

pip install -r requirements.txt

python manage.py makemigrations
python manage.py migrate
python manage.py shell
exec(open('MongoScript.py').read())
quit()
python manage.py runserver


Server can be acessed at: http://localhost:8000/


- To create a manager account who can acess the website functionalities:
python manage.py createsuperuser

-Use Django Admin Page to have more power controlling the DB Data (http://localhost:8000/admin)
-Log with the same superuser created above

-We don't have a way of obtaining regions, sanitary districts and streets yet.
-So this have to be manually inserted using the Django Admin Page to run the scripts and API properly
- MongoScript.py provides a quick way of creating some tables 
-A better approach will be developed in the future


-At the Django Admin Page 
    Add Distrito sanitarios 
    --------------------------------------------------------
    | numDistrito == District ID                           |
    | QntEstratos == Amount of Regions in the district     |
    --------------------------------------------------------


    Add Bairros(Streets)
    --------------------------------------------------------
    |NomeBairro == Same Street name used in the Mobile App          |
    |Data == Date (A street may be reallocated into another region, this occassionaly happens throught out the years 
    |DistritoSanitario == Sanitary districted where this street is located
    |IdEstrato == Region ID (Can't be a higher num than the specified in the Distritos sanitarios' QntEstratos)
    ----------------------------------------------------------

    Add Visita formulario liras
    -One can make a LIRAa Form for test purporses

To store a LIRAa or PSA Form from the mobile app in the back-end, a registered Street must be used
