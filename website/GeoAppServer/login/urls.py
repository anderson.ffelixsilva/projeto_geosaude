from django.urls import path, re_path
from django.contrib.auth.views import LoginView ,LogoutView 

from . import views

urlpatterns = [
    path('', LoginView.as_view(template_name='login/login_pagina.html'), name='login_page'),
    path('usuario/', views.usuarioListView, name='usuario'),
    path('cadastro/', views.cadastroView, name='cadastro'),
    re_path(r'^editar/(?P<userId>[\d]+)/$', views.editarUsuarioView, name='editar_usuario'),
    re_path(r'^metas/(?P<userId>[\d]+)/$', views.proporMetasPage, name='propor_metas'),
    re_path(r'^liraa/(?P<userId>[\d]+)/$', views.liraListView, name='liraa_lista'),
    re_path(r'^psa/(?P<userId>[\d]+)/$', views.psaListView, name='psa_lista'),
    path('consolidados/', views.consolidadosPage, name='consolidados'),
    path('logout/', LogoutView.as_view(next_page="login_page"), name='logout'),
]