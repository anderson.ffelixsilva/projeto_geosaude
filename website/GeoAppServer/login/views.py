from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.urls import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required, user_passes_test
from django.views import generic
from django.utils.decorators import method_decorator

#Models e Forms
from api.models import VisitaFormularioLira, VisitaFormularioPSA, DistritoSanitario 
from .forms import CadastroForm, UserEditForm, MetasForm, ConsolidadosForm
from .models import AgenteDeSaude, Metas

#Libs
from . import consolidados #Script de Criação dos Consolidados
import xlsxwriter          #Biblioteca para criação de Planilhas Excel


def user_is_staff(user):
	return user.is_staff


#Tela de Perfil
@method_decorator(user_passes_test(user_is_staff), name='dispatch')
class UsuarioListView(generic.ListView):

	queryset = AgenteDeSaude.objects.filter(is_staff=False).order_by('nome')
	template_name = 'login/perfil_adm.html'
	context_object_name = 'user_list'
	paginate_by = 10


#Tela de Cadastro     
@method_decorator(user_passes_test(user_is_staff), name='dispatch')
class CadastroView(generic.CreateView):

	model = AgenteDeSaude
	template_name = 'login/cadastro.html'
	success_url = reverse_lazy('usuario')
	form_class = CadastroForm


#Edição de Usuario
@method_decorator(user_passes_test(user_is_staff), name='dispatch')
class EditarUsuarioView(generic.UpdateView):

	model = AgenteDeSaude
	template_name = 'login/editar_usuario.html'
	form_class = UserEditForm
	success_url = reverse_lazy('usuario')

	def get_object(self, **kwargs):
		return AgenteDeSaude.objects.get(pk=self.kwargs['userId'])


#Lista de Formulários LiraA
@method_decorator(user_passes_test(user_is_staff), name='dispatch')
class LiraAListView(generic.ListView):

	template_name = 'login/formularios.html'
	context_object_name = 'formulario_list'
	paginate_by = 15
	
	def get_queryset(self):
		return VisitaFormularioLira.objects.filter(agenteDeSaude__id= self.kwargs['userId'])

	def get_context_data(self,**kwargs):
		context = super().get_context_data(**kwargs)
		context['formulario'] = 'Formulário LiraA'
		return context


#Lista de Formulários PSA
@method_decorator(user_passes_test(user_is_staff), name='dispatch')
class PSAListView(generic.ListView):

	template_name = 'login/formularios.html'
	context_object_name = 'formulario_list'
	paginate_by = 15
	
	def get_queryset(self):
		return VisitaFormularioPSA.objects.filter(agenteDeSaude__id= self.kwargs['userId'])

	def get_context_data(self,**kwargs):
		context = super().get_context_data(**kwargs)
		context['formulario'] = 'Formulário PSA'
		return context


#Tela de Impressão dos Consolidados
@user_passes_test(user_is_staff)
def consolidadosPage(request):
	#Recebendo informações, Periodo de análise para criação dos Consolidados
	consolidadosForm = ConsolidadosForm(data=request.POST or None)

	success = False
	if consolidadosForm.is_valid():
		dataInicial = consolidadosForm.cleaned_data.get('dataInicial')
		dataFinal   = consolidadosForm.cleaned_data.get('dataFinal')

		#Criando Arquivo Excel chamado Consolidados.xlsx
		workbook = xlsxwriter.Workbook("Consolidados.xlsx")  
		#Buscando informações sobre os Distritos Sanitarios
		queryDistritos = DistritoSanitario.objects.values()
		#Criando os Consolidados de cada Distrito Sanitario
		for objetoDistrito in queryDistritos:
			consolidados.createConsolidados(workbook,objetoDistrito['numDistrito'],objetoDistrito['qntEstratos'], dataInicial, dataFinal)
		#Fechando arquivo
		workbook.close() 
		success = True

	context = {
		'success': success,
		'consolidadosForm': consolidadosForm,
	}

	return render(request,'login/consolidados.html', context)


#Proposição de Metas
@user_passes_test(user_is_staff)
def proporMetasPage(request, userId):
	# Busca pelo Agente de Saúde informado
	agente = get_object_or_404(AgenteDeSaude, id=userId)

	#Se já existir uma Meta, ela será atualizada
	metas = Metas.objects.filter(idAgente=agente).first()

	metasForm = MetasForm(data= request.POST or None, instance= metas)

	if metasForm.is_valid():
		# Transforma Formulario em um Objeto
		metas = metasForm.save(commit=False)
		# Adiciona o ID do agente de saúde
		metas.idAgente = agente
		# Salva no BD
		metas.save()
		return HttpResponseRedirect( reverse('usuario') )

	context = {
		'agente': agente,
		'metasForm': metasForm,
	}

	return render(request,'login/propor_metas.html', context)





liraListView      = LiraAListView.as_view()
psaListView       = PSAListView.as_view()
usuarioListView   = UsuarioListView.as_view()
cadastroView      = CadastroView.as_view()
editarUsuarioView = EditarUsuarioView.as_view()