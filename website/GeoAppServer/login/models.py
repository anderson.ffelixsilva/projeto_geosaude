from django.db import models
from django.contrib.auth.models import AbstractUser


#Dados do Usuario
class AgenteDeSaude(AbstractUser):
    nome = models.CharField('Nome do Funcionário ', max_length=200)
    profissao = models.CharField('Cargo', max_length=200)
    posto = models.CharField('Posto de Saúde', max_length=200)
    regiao = models.CharField('Região de atuação', max_length=200, blank=True)

    class Meta:
        verbose_name = "Agente de Saúde"
        verbose_name_plural = "Agentes de Saúde"


#Dados utilizados na tabela de Conquistas do Usuário
class Metas(models.Model):
    idAgente = models.ForeignKey('login.AgenteDeSaude', on_delete=models.CASCADE)
    dataCriação = models.DateTimeField('Data Inicial', auto_now_add=True)
    dataLimite = models.DateTimeField ('Data Limite', blank=True)
    numVisitas = models.IntegerField('Meta de Visitas')
    numVisitasFeitas = models.IntegerField('Visitas Realizadas', default=0)

    class Meta:
        verbose_name = "Meta"
        verbose_name_plural = "Metas"
