from django.db import models
from login.models import AgenteDeSaude 
from api.models import Bairros

#Populando nova Instancia a partir do JSON
def populateLiraForm (LiraFormInstance,dataJSON,agente):
      
    #Causa erro caso uma destas informações não esteja presente
    if 'places' not in dataJSON:
        return False
    if 'immobile' not in dataJSON:
        return False
    if 'dateVisit' not in dataJSON['immobile']:
        return False
    if 'tpImmobile' not in dataJSON['immobile']:
        return False

    #Informações sobre o Imovel
    LiraFormInstance.localImovel = dataJSON['places']
    LiraFormInstance.dataVisita = dataJSON['immobile']['dateVisit']

    #Deriva codigo do Tipo de Imóvel
    immobileCode = immobileType(dataJSON['immobile']['tpImmobile'])
    if immobileCode == -1:
        return False

    LiraFormInstance.tipoImovel = immobileCode

    #Identificação da Rua e Endereço
    rua, endereco = dataJSON['places'].split(',') 
    
    #Procura informações sobre a rua/bairro 
    #Devido a possibilidade de um bairro/rua mudar de Estrato, armazenamos varios dados da mesma Rua em datas diferentes.
    #Iremos procurar a linha(rua/bairo) cuja data seja mais proxima da data do Formulario  
    bairroObject = Bairros.objects.filter(nomeBairro = rua,data__lte=dataJSON['immobile']['dateVisit']).order_by('-data').first()
    #Caso a data do formulario seja mais antiga que qualquer registro do BD, iremos usar o registro mais antigo
    if bairroObject is None:
        bairroObject = Bairros.objects.filter(nomeBairro = rua).order_by('-data').first();


    #Distrito Sanitario e Estrato
    LiraFormInstance.estrato = bairroObject.idEstrato
    LiraFormInstance.distritoSanitario = bairroObject.distritoSanitario.numDistrito

    #Agente de Saúde Responsavel
    LiraFormInstance.agenteDeSaude = agente

    #Vetores Aedes
    if 'vtAedes' in dataJSON and dataJSON['vtAedes'] is not None: #Checa a existência da chave no Dicionario
        if 'A1' in dataJSON['vtAedes']:
            LiraFormInstance.aedesA1 = dataJSON['vtAedes']['A1'] 
        if 'A2' in dataJSON['vtAedes']:
            LiraFormInstance.aedesA2 = dataJSON['vtAedes']['A2']
        if 'B' in dataJSON['vtAedes']:
            LiraFormInstance.aedesB = dataJSON['vtAedes']['B']
        if 'C' in dataJSON['vtAedes']:
            LiraFormInstance.aedesC = dataJSON['vtAedes']['C']
        if 'D1' in dataJSON['vtAedes']:
            LiraFormInstance.aedesD1 = dataJSON['vtAedes']['D1']
        if 'D2' in dataJSON['vtAedes']:
            LiraFormInstance.aedesD2 = dataJSON['vtAedes']['D2']
        if 'E' in dataJSON['vtAedes']:
            LiraFormInstance.aedesE = dataJSON['vtAedes']['E']
    
    #Larvicidas
    if 'larvicides' in dataJSON and dataJSON['larvicides'] is not None:
        if 'BTiG_depositos' in dataJSON['larvicides']:
            LiraFormInstance.larvicidaBTiG_depositos  =  dataJSON['larvicides']['BTiG_depositos']
        if 'BTiG_gramas' in dataJSON['larvicides']:
            LiraFormInstance.larvicidaBTiG_gramas  =  dataJSON['larvicides']['BTiG_gramas']
        if 'BTiWDg_depositos' in dataJSON['larvicides']:
            LiraFormInstance.larvicidaBTiWDg_depositos = dataJSON['larvicides']['BTiWDg_depositos']
        if 'BTiWDg_gramas' in dataJSON['larvicides']:    
            LiraFormInstance.larvicidaBTiWDg_gramas  =  dataJSON['larvicides']['BTiWDg_gramas']

    #Depositos Eliminados
    if 'depositsRemoved' in dataJSON and dataJSON['depositsRemoved'] is not None:
        LiraFormInstance.depositosEliminados = dataJSON['depositsRemoved']

    return LiraFormInstance



#As funções abaixo mapeiam os valores presente no JSON para a legenda usada pela Prefeitura
def immobileType(type):
    if type == 'residence':
        return 1
    elif type == 'commerce':
        return 2
    elif type == 'others':
        return 3
    elif type == 'wasteland':
        return 4
    else:
        return -1