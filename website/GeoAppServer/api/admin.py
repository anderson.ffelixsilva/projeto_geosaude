from django.contrib import admin
from .models import VisitaFormularioLira, VisitaFormularioPSA, DistritoSanitario, Bairros, Visitas
# Register your models here.

@admin.register(VisitaFormularioLira)
class LiraAdmin(admin.ModelAdmin):
    list_display = ("localImovel","dataVisita","agenteDeSaude")

@admin.register(VisitaFormularioPSA)
class PSAAdmin(admin.ModelAdmin):
    list_display = ("localImovel","dataVisita","agenteDeSaude")

@admin.register(DistritoSanitario)
class DistritoSanitarioAdmin(admin.ModelAdmin):
    list_display = ("numDistrito","qntEstratos")

@admin.register(Bairros)
class BairrosAdmin(admin.ModelAdmin):
    list_display = ("nomeBairro","idEstrato","get_distritoID","data")

    def get_distritoID(self,obj):
        return obj.distritoSanitario.numDistrito
    get_distritoID.short_description = 'Distrito Sanitário'


admin.site.register(Visitas)