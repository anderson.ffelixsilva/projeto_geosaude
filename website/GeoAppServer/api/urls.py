from django.urls import path
from api import views

app_name = 'GeoAppAPI'

urlpatterns = [
    path('login/', views.apiLogin, name='apiLogin'),
    path('formularioLIRAa/', views.formulario, {'formType': 'lira'}, name='formLIRAa'),
    path('formularioPSA/',   views.formulario, {'formType': 'psa'} , name='formPSA'),
]