#Libs do Django
from django.shortcuts import render
from django.contrib.auth import authenticate,login
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt

#Models and Form
from login.models import AgenteDeSaude, Metas
from .models import VisitaFormularioLira, VisitaFormularioPSA, Bairros, DistritoSanitario, Visitas
from .liraForm import populateLiraForm
from .psaForm import populatePSAForm

#Libs Externas
import json, jwt
from datetime import date, datetime

#Palavra-Chave usada como segredo para criptografar o Token de Autenticação
tokenSecretWord = "geoappsecret"

#Autenticação do Usuário pelo aplicativo móvel
@csrf_exempt
def apiLogin(request):
    if request.method == "POST":
        #Extração dos dados de login enviados pelo Usuario
        dataJSON = json.loads(request.body)

        #Checagem da existência de campos
        if 'username' not in dataJSON:
            return HttpResponseBadRequest("Login/Password Required")
        if 'senha' not in dataJSON:
            return HttpResponseBadRequest("Login/Password Required")

        #Tentativa de autenticação
        user = authenticate(username=dataJSON['username'], password=dataJSON['senha']) 
        
        #Em caso de sucesso, cria um JSON com as informações necessárias
        if user is not None:   
            #Criando Token de Autenticação
            #O token possui informações sobre o Usuário que podem ser usadas para identifica-lo
            payload = {'username': user.username} #Informação sobre Usuario
            authToken = jwt.encode(payload,tokenSecretWord) #Cifrando informação

            #authToken é uma cadeia de bytes, para envia-lo pelo JSON é preciso transforma-lo em String
            authTokenString = authToken.decode("utf-8") #Transformando cadeia de bytes em String

            #Metas que o usuario deve comprir
            try:
                metas = Metas.objects.get(idAgente=user)
            except Metas.DoesNotExist:
                metas = None
                
            if metas is None:
                metas = Metas(numVisitas=0,numVisitasFeitas=0,dataLimite=datetime.now())

            #6 Bairros mais visitados pelo Agente
            rank_visitas = Visitas.objects.filter(idAgente=user).order_by('-quantidade')[:6]

            visitas_list = []
            for visitas in rank_visitas:
                visitas_list.append({'bairro':visitas.bairro.nomeBairro, 'visitasFeitas': visitas.quantidade})
            

            #Dicionario com Dados do Usuario 
            dadosUsuario = {
                'auth_token': authTokenString, 'nome': user.nome, 'profissao': user.profissao, 'posto': user.posto,
                'metas' : {
                    'numVisitas': metas.numVisitas,
                    'numVisitasFeitas': metas.numVisitasFeitas, 
                    'dataLimite' : str(metas.dataLimite.date())
                },
                'visitas': visitas_list

            }

            print(dadosUsuario)
            
            #Criando JSON apartir do Dicionario
            dadosJson = json.dumps(dadosUsuario)
        
            return HttpResponse(dadosJson, content_type="application/json") #Retorna o JSON criado      

        else:
            return HttpResponseBadRequest('User not found.')    #Erro de Login
    else:
        return HttpResponse("Envie uma requisição com método POST")


#Captura e armazenamento dos Formulários enviados pelo APP móvel
@csrf_exempt 
def formulario(request, formType):
    if request.method == "POST":
        #Realiza a autenticação do Usuario
        user = auth(request)
        if user is None:
            return HttpResponseBadRequest("Authentication Error / User not found.")

        #Extração de dados enviados pelo APP Móvel
        data = json.loads(request.body)

        #Cria instância do Formulario
        if formType == 'lira':
            form = VisitaFormularioLira()
            #Checa entradas e popula a instância com dados do JSON
            form = populateLiraForm(form,data,user)
        
        elif formType == 'psa':
            form = VisitaFormularioPSA()
            #Checa entradas e popula a instância com dados do JSON
            form = populatePSAForm(form,data,user)
            
        #Em caso de Entradas inválidas, retorna um erro
        if form == False:
            return HttpResponseBadRequest("Entradas mal-formatadas ou inválidas")

        #Salva instancia no Banco de Dados
        form.save()
            
        #Atualiza conquistas do Agente 
        atualizarMetas(user)
        atualizarVisitasAgente(form)

        return HttpResponse("Formulário armazenada com Sucesso")
    
    #Mensagem de erro caso não seja enviada uma requisição POST
    else:
        return HttpResponse("Envie uma requisição com método POST")


#Autenticação do Usuario através do Token de Autenticação
def auth(request):
    #Checa a existência de um Authorization Token no Header do HTTP
    if 'HTTP_AUTHORIZATION' not in request.META:
        return None

    #Token de Autenticação
    authToken = request.META['HTTP_AUTHORIZATION']
    
    #Decifra o Token
    payload = jwt.decode(bytes(authToken,'utf-8'),tokenSecretWord)

    #Utiliza as informações presentes no Token para realizar a autenticação
    user = AgenteDeSaude.objects.get(username=payload['username'])

    return user


#Atualizar Meta do Agente ao receber um novo Formulário
def atualizarMetas(user):
    try:
        metas = Metas.objects.get(idAgente=user)
    except Metas.DoesNotExist:
        return

    metas.numVisitasFeitas += 1
    metas.save()
    

#Atualiza Tabela usada para o Rank de Visitas
def atualizarVisitasAgente(formulario):
    #Recupera Objeto Bairro através do formulario enviado
    bairro = getBairroObject(formulario)

    if bairro is False:
        return

    try:
        visitas = Visitas.objects.get(idAgente=formulario.agenteDeSaude,bairro=bairro)
    except Visitas.DoesNotExist:
        visitas = Visitas(idAgente=formulario.agenteDeSaude,bairro=bairro)

    visitas.quantidade = visitas.quantidade + 1
    visitas.save()


def getBairroObject(formulario):
    bairro = False

    try:
        distrito  = DistritoSanitario.objects.get(
            numDistrito=formulario.distritoSanitario
        )
    except DistritoSanitario.DoesNotExist:
        print("Distrito especificado no Formulario não existe")
        return False

    try:
        bairro  = Bairros.objects.filter(
            idEstrato=formulario.estrato,distritoSanitario=distrito
        ).order_by('-data')[0]
    except Bairros.IndexError:
        print("Bairro especificado no Formulario não existe")
        return False
    return bairro