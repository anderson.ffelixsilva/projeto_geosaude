from django.db import models
from login.models import AgenteDeSaude

#Tabela de formularios PSA enviados pelos agentes
class VisitaFormularioPSA (models.Model):
    #Chaves Estrangeiras
    agenteDeSaude = models.ForeignKey('login.AgenteDeSaude',null=True, on_delete=models.SET_NULL,verbose_name="Agente de Saúde")
    
    #Dados do Imóvel
    localImovel = models.CharField(max_length=200,verbose_name="Endereço")
    dataVisita = models.DateField(verbose_name="Data da Visita")
    situacaoImovel = models.PositiveIntegerField(verbose_name="Situação")
    tipoImovel = models.PositiveIntegerField(verbose_name="Tipo do Imóvel")

    #Região e Estrato
    distritoSanitario = models.PositiveIntegerField(verbose_name="Distrito Sanitário")
    estrato = models.PositiveIntegerField(verbose_name="Estrato")

    #Lixo
    lixoTipo = models.PositiveIntegerField(verbose_name="Tipo do Lixo")
    lixoSituacao = models.PositiveIntegerField(verbose_name="Situação do Lixo")
    lixoTratamento = models.PositiveIntegerField(verbose_name="Tratamento")

    #Criadouros Aedes
    aedesA1 = models.PositiveIntegerField(default=0)
    aedesA2 = models.PositiveIntegerField(default=0)
    aedesB = models.PositiveIntegerField(default=0)
    aedesC = models.PositiveIntegerField(default=0)
    aedesD1 = models.PositiveIntegerField(default=0)
    aedesD2 = models.PositiveIntegerField(default=0)
    aedesE = models.PositiveIntegerField(default=0)
    
    #Tratamento Aedes
    aedesPositivo = models.PositiveIntegerField(default=0)
    aedesMecanico = models.PositiveIntegerField(default=0)
    aedesBiologico = models.PositiveIntegerField(default=0)
    aedesQuimico = models.PositiveIntegerField(default=0)

    #Criadouros Culex
    culexFosso = models.BooleanField(default=False)
    culexCisterna = models.BooleanField(default=False)
    culexCanal = models.BooleanField(default=False)
    culexCharco = models.BooleanField(default=False)
    culexDrenagem = models.BooleanField(default=False)

    #Tratamento Culex
    culexPositivo = models.PositiveIntegerField(default=0)
    culexMecanico = models.PositiveIntegerField(default=0)
    culexBiologico = models.PositiveIntegerField(default=0)
    culexQuimico = models.PositiveIntegerField(default=0)

    #Ovitrampas
    ovitrampas = models.PositiveIntegerField(default=0,verbose_name="Ovitrampas")

    #Larvas e Larvicidas
    larvicidaBTiG_depositos = models.PositiveIntegerField(default=0)
    larvicidaBTiG_gramas = models.PositiveIntegerField(default=0)
    larvicidaBTiWDg_depositos = models.PositiveIntegerField(default=0)
    larvicidaBTiWDg_gramas= models.PositiveIntegerField(default=0)
    larvicidaBsG_depositos = models.PositiveIntegerField(default=0)
    larvicidaBsG_gramas = models.PositiveIntegerField(default=0)

    #MetaDados
    class Meta: 
        verbose_name = "Formulário PSA"
        verbose_name_plural = "Formulários PSA"

    

#Tabela de formularios LiraA enviados pelos agentes
class VisitaFormularioLira(models.Model):

    #Chaves Estrangerias
    agenteDeSaude = models.ForeignKey('login.AgenteDeSaude',null=True,on_delete=models.SET_NULL,verbose_name="Agente de Saúde")

    #Dados do Imóvel
    localImovel = models.CharField(max_length=200, verbose_name="Endereço")
    dataVisita = models.DateField(verbose_name="Data da Visita")
    tipoImovel = models.PositiveIntegerField(verbose_name="Tipo do Imóvel")

    #Região e Estrato
    distritoSanitario = models.PositiveIntegerField(verbose_name="Distrito Sanitário")
    estrato = models.PositiveIntegerField(verbose_name="Estrato")

    #Criadouros Aedes
    aedesA1 = models.PositiveIntegerField(default=0)
    aedesA2 = models.PositiveIntegerField(default=0)
    aedesB = models.PositiveIntegerField(default=0)
    aedesC = models.PositiveIntegerField(default=0)
    aedesD1 = models.PositiveIntegerField(default=0)
    aedesD2 = models.PositiveIntegerField(default=0)
    aedesE = models.PositiveIntegerField(default=0)

    #Larvas e Larvicidas
    larvicidaBTiG_depositos = models.PositiveIntegerField(default=0)
    larvicidaBTiG_gramas = models.PositiveIntegerField(default=0)
    larvicidaBTiWDg_depositos = models.PositiveIntegerField(default=0)
    larvicidaBTiWDg_gramas= models.PositiveIntegerField(default=0)

    #Depositos Eliminados
    depositosEliminados = models.PositiveIntegerField(default=0,verbose_name="Depósitos Eliminados")

    #MetaDados
    class Meta: 
        verbose_name = "Formulário LiraA"
        verbose_name_plural = "Formulários LiraA"


class DistritoSanitario(models.Model):
    numDistrito = models.PositiveIntegerField(unique=True,verbose_name="ID Distrito") #Id do Distrito
    qntEstratos = models.PositiveIntegerField(default=0,verbose_name="Quantidade de Estratos")   #Quantidade de Estratos por distrito

    def __str__(self):
        return "Distrito Sanitário " + str(self.numDistrito)

    #MetaDados
    class Meta:
        verbose_name = "Distrito Sanitário"
        verbose_name_plural = "Distritos Sanitário"


class Bairros(models.Model):
    nomeBairro = models.CharField(max_length=80, verbose_name="Nome")
    data = models.DateField(verbose_name="Data de Cadastro")
    idEstrato = models.PositiveIntegerField(verbose_name="Estrato") #Id do Estrato
    distritoSanitario = models.ForeignKey('api.DistritoSanitario',on_delete='cascade',verbose_name="Distrito Sanitário")


    def __str__(self):
        return self.nomeBairro

    #MetaDados
    class Meta:
        verbose_name = "Bairro"
        verbose_name_plural = "Bairros"

    
class Visitas(models.Model):

    idAgente = models.ForeignKey('login.AgenteDeSaude',on_delete=models.CASCADE,verbose_name='Agente de Saúde')
    bairro = models.ForeignKey('api.Bairros', on_delete=models.CASCADE, verbose_name='Bairro')
    quantidade = models.PositiveIntegerField(default=0,verbose_name='Número de Visitas')

    class Meta:
        verbose_name = "Visita"
        verbose_name_plural = "Visitas"
        unique_together = (('idAgente','bairro'),)
    
    def __str__(self):
        return "Visitas - {0} - {1}".format(self.idAgente.nome, self.bairro.nomeBairro)
