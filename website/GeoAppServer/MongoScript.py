from api.models import DistritoSanitario, Bairros
from datetime import date

#Script to be used Once, after Django Migrations
# python manage.py shell
# exec(open('MongoScript.py').read())
# quit()

#Creating Sanitary Districts
distrito1 = DistritoSanitario(numDistrito=1,qntEstratos=6)
distrito1.save()

distrito2 = DistritoSanitario(numDistrito=2,qntEstratos=15)
distrito2.save()

distrito3 = DistritoSanitario(numDistrito=3,qntEstratos=7)
distrito3.save()

distrito4 = DistritoSanitario(numDistrito=4,qntEstratos=13)
distrito4.save()

distrito5 = DistritoSanitario(numDistrito=5,qntEstratos=16)
distrito5.save()

distrito6 = DistritoSanitario(numDistrito=6,qntEstratos=6)
distrito6.save()

distrito7 = DistritoSanitario(numDistrito=7,qntEstratos=12)
distrito7.save()

distrito8 = DistritoSanitario(numDistrito=8,qntEstratos=9)
distrito8.save()

#Creating Streets
Bairros(nomeBairro="Rua Marquês de Baipendi",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Rua Cirilino Afonso de Melo",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Rua Mário Sete",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Rua São Caetano",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Av. Agamenon Magalhães",data=date.today(),idEstrato=1,distritoSanitario=distrito3).save()
Bairros(nomeBairro="Rua Guaianazes",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Rua Esberard",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Rua Pereira Passos",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Rua Nova",data=date.today(),idEstrato=1,distritoSanitario=distrito1).save()
Bairros(nomeBairro="Rua Projetada",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()
Bairros(nomeBairro="Rua Ledinha",data=date.today(),idEstrato=16,distritoSanitario=distrito2).save()


#Bairros do Distrito Sanitario IV
Bairros(nomeBairro="Ilha do Retiro e Madalena",data=date.today(),idEstrato=1,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Cordeiro 1 e Zumbi",data=date.today(),idEstrato=2,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Cordeiro 2",data=date.today(),idEstrato=3,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Torre",data=date.today(),idEstrato=4,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Prado",data=date.today(),idEstrato=5,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Torrôes",data=date.today(),idEstrato=6,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Engenho do Meio",data=date.today(),idEstrato=7,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Iputinga 1",data=date.today(),idEstrato=8,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Várzea 1 (CDU) / Várzea (Brasilit) / CDU",data=date.today(),idEstrato=9,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Várzea 3 (Baixo) / Várzea 4 (Barreiras)",data=date.today(),idEstrato=10,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Várzea 5 (UR-7)",data=date.today(),idEstrato=11,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Caxangá",data=date.today(),idEstrato=12,distritoSanitario=distrito4).save()
Bairros(nomeBairro="Iputinga 2",data=date.today(),idEstrato=13,distritoSanitario=distrito4).save()

